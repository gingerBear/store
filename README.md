# StoreService

How to start the StoreService application
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/store-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`


**Migrate tables to schema**

Avant tout, faire ```` mvn clean install ````

To migrate the Liquibase schema to the postgres container, run in microservice directory 
```` java -jar target/microservice-1.0-SNAPSHOT.jar db migrate config.yml ````