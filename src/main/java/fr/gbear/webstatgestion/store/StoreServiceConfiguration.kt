package fr.gbear.webstatgestion.store

import io.dropwizard.Configuration
import io.dropwizard.db.DataSourceFactory
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration
import javax.validation.Valid
import javax.validation.constraints.NotNull


class StoreServiceConfiguration : Configuration() {
    var swagger : SwaggerBundleConfiguration? = null
    @Valid
    @NotNull
    var database = DataSourceFactory()

    @Valid
    @NotNull
    var entityPackage: String = ""

    @Valid
    @NotNull
    var resourcePackage: String = ""

    @Valid
    @NotNull
    var daoPackage: String = ""

}