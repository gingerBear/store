package fr.gbear.webstatgestion.store.service

import fr.gbear.webstatgestion.store.db.entity.Store

interface StoreService {
    /**
     * Allows to create a new store.
     * @param store
     * @return store
     */

    fun createStore(store: Store): Store

    /**
     * Allow to update a store
     * @param store
     * @return store
     */
    fun updateStore(store: Store): Store

    /**
     * Allow to get stores
     * @return List<store>
     */
    fun getStores(): List<Store>

    /**
     * Allow to get a store
     * @param id
     * @return Store
     */
    fun getStore(id : Long): Store
}