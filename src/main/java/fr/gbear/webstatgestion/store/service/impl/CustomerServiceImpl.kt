package fr.gbear.webstatgestion.store.service.impl

import fr.gbear.webstatgestion.store.db.dao.BookingDao
import fr.gbear.webstatgestion.store.db.dao.EquipmentDao
import fr.gbear.webstatgestion.store.db.dto.EquipmentsBookingCustomerDto
import fr.gbear.webstatgestion.store.db.mapper.EquipmentMapper
import fr.gbear.webstatgestion.store.service.CustomerService
import fr.gbear.webstatgestion.store.service.EquipmentService
import fr.gbear.webstatgestion.store.service.ModelService
import org.hibernate.SessionFactory

class CustomerServiceImpl(sessionFactory: SessionFactory? = null) : CustomerService {
    private var modelService: ModelService = ModelServiceImpl(sessionFactory)
    private var equipmentDao: EquipmentDao = EquipmentDao.getInstance(sessionFactory)
    private var bookingDao: BookingDao = BookingDao.getInstance(sessionFactory)
    private var equipmentMapper = EquipmentMapper.getInstance()
    private var equipmentService: EquipmentService

    init {
        equipmentDao = EquipmentDao.getInstance(sessionFactory)
        modelService = ModelServiceImpl(sessionFactory)
        equipmentService = EquipmentServiceImpl(sessionFactory)
    }

    override fun getBookingEquipmentsCustomer(idCustomer: Long): List<EquipmentsBookingCustomerDto> {
        val bookings = bookingDao.findAllBookingByCustomer(idCustomer)
        return bookings.map {
            val equipmentBooking = EquipmentsBookingCustomerDto()
            equipmentBooking.idBooking = it.idBooking
            equipmentBooking.idCustomer = it.idCustomer
            equipmentBooking.equipments = it.bookingsEquipments.map { bookingEquipment ->
                val equipment = equipmentService.findEquipment(equipmentId = bookingEquipment.idEquipment!!)
                val equipmentBookingDto = equipmentMapper.convertEquipmentToEquipmentBookingDto(equipment)
                equipmentBookingDto.beginDate = bookingEquipment.startDate!!
                equipmentBookingDto.endDate = bookingEquipment.endDate!!
                equipmentBookingDto.insurancesSubscribes = bookingEquipment.insurance
                equipmentBookingDto

            }
            equipmentBooking
        }
    }
}