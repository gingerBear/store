package fr.gbear.webstatgestion.store.service

import fr.gbear.webstatgestion.store.db.dto.ModelEditDto
import fr.gbear.webstatgestion.store.db.entity.Model


interface ModelService {
    /**
     * Allows to create a new modelEdit.
     * @param idStore
     * @param modelEditDto
     * @return modelEdit
     */

    fun createModel(idStore: Long, modelEditDto: ModelEditDto): Model

    /**
     * Allow to update a modelEdit
     * @param modelEdit
     * @param idStore
     * @return modelEdit
     */
    fun updateModel(idStore: Long, modelEdit: ModelEditDto): Model

    /**
     * Allow to get models
     * @param idStore
     * @return List<model>
     */
    fun getModels(idStore: Long): List<Model>

    /**
     * Allow to get model
     * @param idModel
     * @param idStore
     * @return Model
     */
    fun getModelById(idStore: Long, idModel: Long): Model
}