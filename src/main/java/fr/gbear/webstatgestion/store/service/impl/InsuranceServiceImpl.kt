package fr.gbear.webstatgestion.store.service.impl

import fr.gbear.webstatgestion.store.db.dao.InsuranceDao
import fr.gbear.webstatgestion.store.db.entity.Insurance
import fr.gbear.webstatgestion.store.service.InsuranceService
import fr.gbear.webstatgestion.store.util.exception.insurance.InsuranceCreationException
import fr.gbear.webstatgestion.store.util.exception.insurance.InsuranceFindException
import fr.gbear.webstatgestion.store.util.exception.insurance.InsuranceUpdateException
import org.hibernate.SessionFactory

class InsuranceServiceImpl(sessionFactory: SessionFactory? = null) : InsuranceService {

    private val storeService = StoreServiceImpl(sessionFactory)
    private var insuranceDao = InsuranceDao.getInstance(sessionFactory)

    override fun createAnInsurance(storeId: Long, insurance: Insurance): Insurance {
        storeService.getStore(storeId)
        checkInsurance(storeId, insurance).takeIf { b -> b }
                ?: throw InsuranceCreationException("Choose between price or rate")
        val insuranceOpt = insuranceDao.insert(insurance)
        insuranceOpt.orElseThrow { throw InsuranceCreationException() }
        return insuranceOpt.get()
    }

    override fun updateAnInsurance(storeId: Long, insurance: Insurance): Insurance {
        val oldInsurance = getInsurance(storeId, insurance.idInsurance!!)
        checkInsurance(storeId, insurance).takeIf { b -> b }
                ?: throw InsuranceUpdateException("Choose between price or rate")
        oldInsurance.insuranceRate = insurance.insuranceRate
        oldInsurance.price = insurance.price
        oldInsurance.isActive = insurance.isActive
        oldInsurance.name = insurance.name
        val insuranceOpt = insuranceDao.update(oldInsurance)
        insuranceOpt.orElseThrow { throw InsuranceUpdateException() }
        return insuranceOpt.get()
    }

    override fun getInsurances(storeId: Long): List<Insurance> {
        storeService.getStore(storeId)
        return insuranceDao.findAllInsurances(storeId)
    }

    override fun deleteInsurance(storeId: Long, insuranceId: Long): Insurance {
        val insurance = getInsurance(storeId, insuranceId)
        val insuranceOpt = insuranceDao.delete(insurance.idInsurance!!)
        insuranceOpt.takeIf { optional -> optional.isPresent } ?: throw Exception()
        return insuranceOpt.get()
    }

    override fun getInsurance(storeId: Long, insuranceId: Long): Insurance {
        storeService.getStore(storeId)
        val insuranceOpt = insuranceDao.findById(insuranceId).takeIf { optional -> optional.isPresent }
                ?: throw InsuranceFindException()
        return insuranceOpt.get()
    }

    override fun checkInsurance(storeId: Long, insurance: Insurance): Boolean {
        return insurance.idStore == storeId && ((insurance.insuranceRate != null && insurance.price == null) || (insurance.insuranceRate == null && insurance.price != null))
    }
}