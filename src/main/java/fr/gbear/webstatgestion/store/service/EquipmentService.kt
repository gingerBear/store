package fr.gbear.webstatgestion.store.service

import fr.gbear.webstatgestion.store.db.dto.EquipmentBookingEditDto
import fr.gbear.webstatgestion.store.db.dto.EquipmentEditDto
import fr.gbear.webstatgestion.store.db.dto.EquipmentsBookingCustomerDto
import fr.gbear.webstatgestion.store.db.dto.ModelDto
import fr.gbear.webstatgestion.store.db.entity.Equipment
import fr.gbear.webstatgestion.store.db.entity.StatusEquipment
import fr.gbear.webstatgestion.store.db.enumeration.StateEquipment

interface EquipmentService {
    /**
     * Allows to create a new equipment.
     * @param equipmentDto
     * @param storeId
     * @return equipment
     */

    fun createEquipment(storeId: Long, equipmentDto: EquipmentEditDto): Equipment

    /**
     * Allow to update a equipment
     * @param equipmentDto
     * @param stateEquipment
     * @return equipment
     */
    fun updateEquipment(storeId: Long, equipmentDto: EquipmentEditDto, stateEquipment: StateEquipment? = null): Equipment

    /**
     * Allow to get equipments
     * @return List<ModelDto>
     */
    fun getEquipments(storeId: Long): List<ModelDto>

    /**
     * Allow to delete equipment
     * @return List<ModelDto>
     */
    fun deleteEquipment(storeId: Long, equipmentId: Long): Boolean


    /**
     *
     * @param equipment
     * @param stateEquipment
     * @return StatusEquipment
     */
    fun createStatusEquipment(equipment: Equipment, stateEquipment: StateEquipment): StatusEquipment

    /**
     *  Allows to booking equipment
     *  @param bookingEditDtos
     *  @param storeId
     *  @return List<Equipment>
     *
     */
    fun bookingEquipments(storeId: Long, bookingEditDtos: List<EquipmentBookingEditDto>): List<Equipment>


    /**
     *  Allows to get  booking equipment by customer for a store
     *  @param idCustomer
     *  @param storeId
     *  @return List<Equipment>
     *
     */
    fun getBookingEquipmentsCustomerForStore(storeId: Long, idCustomer: Long?): List<EquipmentsBookingCustomerDto>


    fun findEquipment(storeId: Long? = null, equipmentId: Long): Equipment
}