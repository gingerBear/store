package fr.gbear.webstatgestion.store.service.impl

import fr.gbear.webstatgestion.store.db.dao.UserDao
import fr.gbear.webstatgestion.store.db.entity.User
import fr.gbear.webstatgestion.store.service.StoreService
import fr.gbear.webstatgestion.store.service.UserService
import fr.gbear.webstatgestion.store.util.Util
import fr.gbear.webstatgestion.store.util.exception.user.UserCreationException
import fr.gbear.webstatgestion.store.util.exception.user.UserFindException
import fr.gbear.webstatgestion.store.util.exception.user.UserUpdateException
import org.hibernate.SessionFactory

class UserServiceImpl : UserService {

    private var userDao: UserDao
    private var storeService: StoreService

    constructor() {
        userDao = UserDao.getInstance(null)
        storeService = StoreServiceImpl()
    }

    constructor(sessionFactory: SessionFactory) {
        userDao = UserDao.getInstance(sessionFactory)
        storeService = StoreServiceImpl(sessionFactory)
    }

    override fun createUser(user: User): User {
        if (isValid(user)) {
            val userOpt = userDao.insert(user)
            if (userOpt.isPresent) {
                return userOpt.get()
            }
        }
        throw UserCreationException("An error occurred while creating a user\n")
    }

    override fun updateUser(user: User): User {
        if (user.idUser != null && isValid(user)) {
            val userOldOpt = userDao.findById(user.idUser!!)
            if (userOldOpt.isPresent) {
                val userOld = userOldOpt.get()
                userOld.firstName = user.firstName
                userOld.lastName = user.lastName
                userOld.isAdmin = user.isAdmin
                val userOpt = userDao.update(userOld)
                if (userOpt.isPresent) {
                    return userOpt.get()
                }
            } else {
                throw UserFindException("An error occurred while getting a user\n")
            }
        }
        throw UserUpdateException("An error occurred while updating a user\n")
    }

    override fun getUsers(): List<User> {
        return userDao.findAllUser()
    }

    override fun getUserByEmail(email: String): User {
        val userOpt = userDao.findByEmail(email)
        return if (userOpt.isPresent) userOpt.get() else throw UserFindException("An error occurred while getting a user\n")
    }

    private fun isValid(user: User): Boolean {
        return user.arrivalDate != null && Util.isValidString(user.firstName) && Util.isValidString(user.lastName)
    }
}