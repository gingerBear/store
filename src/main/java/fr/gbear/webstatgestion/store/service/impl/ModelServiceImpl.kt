package fr.gbear.webstatgestion.store.service.impl

import fr.gbear.webstatgestion.store.db.dao.ModelDao
import fr.gbear.webstatgestion.store.db.dto.ModelEditDto
import fr.gbear.webstatgestion.store.db.entity.Model
import fr.gbear.webstatgestion.store.db.mapper.ModelMapper
import fr.gbear.webstatgestion.store.service.ModelService
import fr.gbear.webstatgestion.store.service.StoreService
import fr.gbear.webstatgestion.store.util.Util
import fr.gbear.webstatgestion.store.util.exception.model.ModelCreationException
import fr.gbear.webstatgestion.store.util.exception.model.ModelFindException
import fr.gbear.webstatgestion.store.util.exception.model.ModelUpdateException
import org.hibernate.SessionFactory

class ModelServiceImpl(sessionFactory: SessionFactory? = null) : ModelService {
    private var modelDao: ModelDao = ModelDao.getInstance(sessionFactory)
    private var storeService: StoreService = StoreServiceImpl(sessionFactory)
    private var modelMapper = ModelMapper.getInstance()


    override fun createModel(idStore: Long, modelEditDto: ModelEditDto): Model {
        isValid(modelEditDto).takeIf { b -> b }
                ?: throw ModelCreationException("An error occurred while creating a model\n")
        val store = storeService.getStore(idStore)
        val model = modelMapper.modelDtoToModel(modelEditDto)
        model.store = store
        val modelOpt = modelDao.insert(model).takeIf { optional -> optional.isPresent }
                ?: throw ModelCreationException("An error occurred while creating a model\n")
        return modelOpt.get()
    }

    override fun updateModel(idStore: Long, modelEdit: ModelEditDto): Model {
        storeService.getStore(idStore)
        if (modelEdit.idModel != null && isValid(modelEdit)) {
            val modelOldOpt = modelDao.findById(modelEdit.idModel!!)
            if (modelOldOpt.isPresent) {
                val modelOld = modelOldOpt.get()
                modelOld.reference = modelEdit.reference
                modelOld.model = modelEdit.model
                modelOld.barcode = modelEdit.barcode
                modelOld.description = modelEdit.description
                modelOld.brand = modelEdit.brand
                val modelOpt = modelDao.update(modelOld)
                if (modelOpt.isPresent) {
                    return modelOpt.get()
                }
            } else {
                throw ModelFindException("An error occurred while getting a modelEdit\n")
            }
        }
        throw ModelUpdateException("An error occurred while updating a modelEdit\n")
    }

    override fun getModels(idStore: Long): List<Model> {
        storeService.getStore(idStore)
        return modelDao.findAllModel(idStore)
    }

    private fun isValid(modelEdit: ModelEditDto): Boolean {
        return Util.isValidString(modelEdit.reference) && Util.isValidString(modelEdit.model) && modelEdit.barcode != null
    }

    override fun getModelById(idStore: Long, idModel: Long): Model {
        storeService.getStore(idStore)
        val modelOpt = modelDao.findById(idModel)
        return if (modelOpt.isPresent) modelOpt.get() else throw ModelFindException("An error occurred while getting a modelEdit\n")
    }
}