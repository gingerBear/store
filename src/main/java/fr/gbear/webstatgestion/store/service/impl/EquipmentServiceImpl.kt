package fr.gbear.webstatgestion.store.service.impl

import fr.gbear.webstatgestion.store.db.dao.*
import fr.gbear.webstatgestion.store.db.dto.EquipmentBookingEditDto
import fr.gbear.webstatgestion.store.db.dto.EquipmentEditDto
import fr.gbear.webstatgestion.store.db.dto.EquipmentsBookingCustomerDto
import fr.gbear.webstatgestion.store.db.dto.ModelDto
import fr.gbear.webstatgestion.store.db.entity.*
import fr.gbear.webstatgestion.store.db.enumeration.StateEquipment
import fr.gbear.webstatgestion.store.db.mapper.EquipmentMapper
import fr.gbear.webstatgestion.store.service.EquipmentService
import fr.gbear.webstatgestion.store.service.InsuranceService
import fr.gbear.webstatgestion.store.service.ModelService
import fr.gbear.webstatgestion.store.service.StoreService
import fr.gbear.webstatgestion.store.util.Util
import fr.gbear.webstatgestion.store.util.exception.equipment.EquipmentBookingException
import fr.gbear.webstatgestion.store.util.exception.equipment.EquipmentCreationException
import fr.gbear.webstatgestion.store.util.exception.equipment.EquipmentFindException
import fr.gbear.webstatgestion.store.util.exception.equipment.EquipmentStatusCreationException
import fr.gbear.webstatgestion.store.util.exception.model.PriceCreationException
import org.hibernate.SessionFactory
import java.util.*
import javax.transaction.Transactional
import kotlin.collections.HashMap

class EquipmentServiceImpl(sessionFactory: SessionFactory? = null) : EquipmentService {

    private var modelService: ModelService = ModelServiceImpl(sessionFactory)
    private var storeDao: StoreDao = StoreDao.getInstance(sessionFactory)
    private var equipmentDao: EquipmentDao = EquipmentDao.getInstance(sessionFactory)
    private var statusEquipmentDao: StatusEquipmentDao = StatusEquipmentDao.getInstance(sessionFactory)
    private var priceDao: PriceDao = PriceDao.getInstance(sessionFactory)
    private var bookingDao: BookingDao
    private var bookingEquipmentDao: BookingEquipmentDao
    private var equipmentMapper = EquipmentMapper.getInstance()
    private var storeService: StoreService
    private var insuranceService: InsuranceService

    init {
        priceDao = PriceDao.getInstance(sessionFactory)
        bookingDao = BookingDao.getInstance(sessionFactory)
        bookingEquipmentDao = BookingEquipmentDao.getInstance(sessionFactory)
        storeDao = StoreDao.getInstance(sessionFactory)
        equipmentDao = EquipmentDao.getInstance(sessionFactory)
        statusEquipmentDao = StatusEquipmentDao.getInstance(sessionFactory)
        modelService = ModelServiceImpl(sessionFactory)
        storeService = StoreServiceImpl(sessionFactory)
        insuranceService = InsuranceServiceImpl(sessionFactory)
    }


    @Transactional
    override fun createEquipment(storeId: Long, equipmentDto: EquipmentEditDto): Equipment {
        val store = storeService.getStore(storeId)
        val equipment = equipmentMapper.convertEquipmentDtoToEquipment(equipmentDto)
        equipment.store = store
        isValid(equipment).takeIf { valid -> valid } ?: throw EquipmentCreationException()
        val priceOpt = priceDao.insert(equipment.price!!).takeIf { optional -> optional.isPresent }
                ?: throw PriceCreationException("An error occurred while getting a store\n")
        equipment.price = priceOpt.get()
        if (equipment.model?.idModel == null) {
            val model = modelService.createModel(storeId, equipmentDto.modelEditDto!!)
            equipment.model = model
        } else {
            equipment.model = modelService.getModelById(storeId, equipment.model!!.idModel!!)
        }
        val newEquipmentOpt = equipmentDao.insert(equipment).takeIf { optional -> optional.isPresent }
                ?: throw EquipmentStatusCreationException()
        val newEquipment = newEquipmentOpt.get()
        createStatusEquipment(newEquipment, StateEquipment.NEW)
        return newEquipment
    }

    override fun createStatusEquipment(equipment: Equipment, stateEquipment: StateEquipment): StatusEquipment {
        equipment.statusEquipments!!.filter { oldStatus -> oldStatus.status == stateEquipment }
                .takeIf { list -> list.isEmpty() }
                ?: throw EquipmentStatusCreationException()
        val statusEquipment = StatusEquipment()
        statusEquipment.idEquipment = equipment.idEquipment
        statusEquipment.status = stateEquipment
        statusEquipment.date = Date()
        val statusOpt = statusEquipmentDao.insert(statusEquipment).takeIf { optional -> optional.isPresent }
                ?: throw EquipmentStatusCreationException()
        equipment.statusEquipments!!.add(statusOpt.get())
        return statusOpt.get()
    }

    override fun updateEquipment(storeId: Long, equipmentDto: EquipmentEditDto, stateEquipment: StateEquipment?): Equipment {
        val store = storeService.getStore(storeId)
        val equipment = equipmentMapper.convertEquipmentDtoToEquipment(equipmentDto)
        val oldEquipmentOpt = equipmentDao.findById(storeId, equipment.idEquipment
                ?: 0).takeIf { optional -> optional.isPresent }
                ?: throw EquipmentFindException()
        val model = if (equipment.model!!.idModel != null) {
            modelService.updateModel(store.idStore!!, equipmentDto.modelEditDto!!)
        } else {
            modelService.createModel(storeId, equipmentDto.modelEditDto!!)
        }

        val oldEquipment = oldEquipmentOpt.get()
        if (oldEquipment.price!!.price != equipment.price!!.price) {
            val priceOpt = priceDao.insert(equipment.price!!).takeIf { optional -> optional.isPresent }
                    ?: throw PriceCreationException("An error occurred while creation of price\n")
            oldEquipment.price = priceOpt.get()
        }
        oldEquipment.model = model
        oldEquipment.store = store
        oldEquipment.size = equipment.size
        oldEquipment.shoeSize = equipment.shoeSize
        val newEquipmentOpt = equipmentDao.update(oldEquipment).takeIf { optional -> optional.isPresent }
                ?: throw EquipmentStatusCreationException()
        val newEquipment = newEquipmentOpt.get()
        stateEquipment?.let { createStatusEquipment(newEquipment, it) }
        return newEquipment
    }

    override fun getEquipments(storeId: Long): List<ModelDto> {
        storeService.getStore(storeId)
        val models = modelService.getModels(storeId)
        val modelDtos = LinkedList<ModelDto>()
        models.forEach { model: Model ->
            val equipments = equipmentDao.findAllEquipmentByModel(storeId, model.idModel!!)
            modelDtos.add(equipmentMapper.formattedEquipmentRender(model, equipments))
        }
        return modelDtos
    }

    override fun deleteEquipment(storeId: Long, equipmentId: Long): Boolean {
        val equipment = findEquipment(storeId, equipmentId)
        return equipmentDao.deleteEquipment(equipment)
    }

    override fun bookingEquipments(storeId: Long, bookingEditDtos: List<EquipmentBookingEditDto>): List<Equipment> {
        val store = storeService.getStore(storeId)
        val equipments = mutableListOf<Equipment>()
        val bookingMap = HashMap<Long, Booking>()
        bookingEditDtos.forEach { equipmentBookingEditDto: EquipmentBookingEditDto ->
            run {
                val equipment = this.findEquipment(storeId, equipmentBookingEditDto.idEquipment!!)
                equipment.bookings!!.filter { bookingEquipment ->
                    checkDateBooking(equipmentBookingEditDto, bookingEquipment)
                }.size.takeIf { i -> i > 0 }?.apply { throw EquipmentBookingException() }
                val booking = checkBookingAlreadyCustomer(bookingMap, equipmentBookingEditDto.idCustomer!!, store)
                var equipmentBooking = BookingEquipment()
                equipmentBooking.idEquipment = equipment.idEquipment
                equipmentBooking.startDate = equipmentBookingEditDto.beginDate
                equipmentBooking.endDate = equipmentBookingEditDto.endDate
                equipmentBooking.idBooking = booking.idBooking
                equipmentBooking.insurance = equipmentBookingEditDto.insurancesSubscribes.map {
                    insuranceService.getInsurance(storeId, it)
                }.toSet()
                equipmentBooking = bookingEquipmentDao.insert(equipmentBooking).orElseThrow { Exception() }
                equipment.bookings!!.add(equipmentBooking)
                equipments.add(equipment)
            }
        }
        return equipments
    }

    fun checkBookingAlreadyCustomer(bookingMap: HashMap<Long, Booking>, idCustomer: Long, store: Store): Booking {
        var booking = bookingMap[idCustomer]
        if (booking != null) return booking
        booking = Booking()
        booking.idCustomer = idCustomer
        booking.store = store
        booking = bookingDao.insert(booking).orElseThrow { EquipmentBookingException() }
        bookingMap[idCustomer] = booking
        return booking
    }

    fun checkDateBooking(equipmentBookingEditDto: EquipmentBookingEditDto, bookingEquipment: BookingEquipment): Boolean {
        val dateBegin = Util.formatterDate(equipmentBookingEditDto.beginDate)
        val endDate = Util.formatterDate(equipmentBookingEditDto.endDate)
        val bookingEquipmentDateBegin = Util.formatterDate(bookingEquipment.startDate!!)
        val bookingEquipmentDateEnd = Util.formatterDate(bookingEquipment.endDate!!)
        return (
                dateBegin == Util.formatterDate(bookingEquipmentDateBegin) || dateBegin == bookingEquipmentDateEnd || endDate == bookingEquipmentDateEnd ||
                        (dateBegin.after(bookingEquipmentDateBegin) && dateBegin.before(bookingEquipmentDateEnd)) ||
                        (endDate.after(bookingEquipmentDateBegin) && endDate.before(bookingEquipmentDateEnd))
                )
    }


    override fun getBookingEquipmentsCustomerForStore(storeId: Long, idCustomer: Long?): List<EquipmentsBookingCustomerDto> {
        storeService.getStore(storeId)
        val bookings = bookingDao.findAllBookingByCustomerAndStore(storeId, idCustomer)
        return bookings.map {
            val equipmentBooking = EquipmentsBookingCustomerDto()
            equipmentBooking.idBooking = it.idBooking
            equipmentBooking.idCustomer = it.idCustomer
            equipmentBooking.equipments = it.bookingsEquipments.map { bookingEquipment ->
                val equipment = findEquipment(storeId, bookingEquipment.idEquipment!!)
                val equipmentBookingDto = equipmentMapper.convertEquipmentToEquipmentBookingDto(equipment)
                equipmentBookingDto.beginDate = bookingEquipment.startDate!!
                equipmentBookingDto.endDate = bookingEquipment.endDate!!
                equipmentBookingDto.insurancesSubscribes = bookingEquipment.insurance
                equipmentBookingDto

            }
            equipmentBooking
        }
    }

    override fun findEquipment(storeId: Long?, equipmentId: Long): Equipment {
        if (storeId != null) storeService.getStore(storeId)
        val equipmentOpt = equipmentDao.findById(storeId, equipmentId).takeIf { optional -> optional.isPresent }
                ?: throw EquipmentFindException()
        return equipmentOpt.get()
    }

    private fun isValid(equipment: Equipment): Boolean {
        return equipment.price!!.price != null
    }
}
