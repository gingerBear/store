package fr.gbear.webstatgestion.store.service

import fr.gbear.webstatgestion.store.db.dto.EquipmentsBookingCustomerDto

interface CustomerService {


    /**
     *  Allows to get  booking equipment by customer
     *  @param idCustomer
     *  @return List<Equipment>
     *
     */
    fun getBookingEquipmentsCustomer(idCustomer: Long): List<EquipmentsBookingCustomerDto>
}