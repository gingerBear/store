package fr.gbear.webstatgestion.store.service

import fr.gbear.webstatgestion.store.db.entity.User

interface UserService {

    /**
     * Allows to create a new user.
     * @param user
     * @return User
     */

    fun createUser(user: User): User

    /**
     * Allow to update a user
     * @param user
     * @return User
     */
    fun updateUser(user: User): User

    /**
     * Allow to get users
     * @return List<User>
     */
    fun getUsers(): List<User>

    /**
     * Allow to get user by his email
     * @return List<User>
     */
    fun getUserByEmail(email: String): User

}