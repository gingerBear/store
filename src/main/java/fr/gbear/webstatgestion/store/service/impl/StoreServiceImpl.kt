package fr.gbear.webstatgestion.store.service.impl

import fr.gbear.webstatgestion.store.db.dao.StoreDao
import fr.gbear.webstatgestion.store.db.entity.Store
import fr.gbear.webstatgestion.store.service.StoreService
import fr.gbear.webstatgestion.store.util.Util
import fr.gbear.webstatgestion.store.util.exception.store.StoreCreationException
import fr.gbear.webstatgestion.store.util.exception.store.StoreFindException
import fr.gbear.webstatgestion.store.util.exception.store.StoreUpdateException
import org.hibernate.SessionFactory

class StoreServiceImpl(sessionFactory: SessionFactory? = null) : StoreService {
    private var storeDao: StoreDao = StoreDao.getInstance(sessionFactory)


    override fun createStore(store: Store): Store {
        if (isValid(store)) {
            val storeOpt = storeDao.insert(store)
            if (storeOpt.isPresent) {
                return storeOpt.get()
            }
        }
        throw StoreCreationException("An error occurred while creating a store\n")
    }

    override fun updateStore(store: Store): Store {
        if (store.idStore != null && isValid(store)) {
            val storeOld = getStore(store.idStore!!)
            storeOld.name = store.name
            storeOld.address = store.address
            storeOld.cp = store.cp
            storeOld.city = store.city
            storeOld.latitude = store.latitude
            storeOld.longitude = store.longitude
            val storeOpt = storeDao.update(storeOld)
            if (storeOpt.isPresent) {
                return storeOpt.get()
            }
        }
        throw StoreUpdateException("An error occurred while updating a store\n")
    }

    override fun getStores(): List<Store> {
        return storeDao.findAllStore()
    }

    override fun getStore(id: Long): Store {
        val storeOpt = storeDao.findById(id)
        return if (storeOpt.isPresent) storeOpt.get() else throw StoreFindException("An error occurred while getting a store\n")

    }

    private fun isValid(store: Store): Boolean {
        return Util.isValidString(store.name) && Util.isValidString(store.address) && Util.isValidString(store.city) && Util.isValidString(store.cp)
    }
}