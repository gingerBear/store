package fr.gbear.webstatgestion.store.service

import fr.gbear.webstatgestion.store.db.entity.Insurance

interface InsuranceService {

    fun createAnInsurance(storeId: Long, insurance: Insurance): Insurance

    fun updateAnInsurance(storeId: Long, insurance: Insurance): Insurance

    fun getInsurances(storeId: Long): List<Insurance>

    fun getInsurance(storeId: Long, insuranceId: Long): Insurance

    fun deleteInsurance(storeId: Long, insuranceId: Long): Insurance

    fun checkInsurance(storeId: Long, insurance: Insurance): Boolean
}