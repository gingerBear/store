package fr.gbear.webstatgestion.store

import com.google.common.collect.ImmutableList
import fr.gbear.webstatgestion.store.util.Util
import io.dropwizard.Application
import io.dropwizard.configuration.EnvironmentVariableSubstitutor
import io.dropwizard.configuration.SubstitutingSourceProvider
import io.dropwizard.db.DataSourceFactory
import io.dropwizard.hibernate.HibernateBundle
import io.dropwizard.hibernate.SessionFactoryFactory
import io.dropwizard.migrations.MigrationsBundle
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import io.federecio.dropwizard.swagger.SwaggerBundle
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration
import org.eclipse.jetty.servlets.CrossOriginFilter
import org.hibernate.SessionFactory
import java.util.*
import javax.servlet.DispatcherType


class StoreServiceApplication : Application<StoreServiceConfiguration>() {
    companion object {
        @JvmStatic
        fun main(args : Array<String>) {
            StoreServiceApplication().run(*args)
        }
    }

    override fun getName(): String {
        return "StoreService"
    }

    private val hibernate = object : HibernateBundle<StoreServiceConfiguration>(getEntities(), SessionFactoryFactory()) {
        override fun getDataSourceFactory(configuration: StoreServiceConfiguration): DataSourceFactory {
            return configuration.database
        }
    }

    override fun initialize(bootstrap: Bootstrap<StoreServiceConfiguration>?) {
        bootstrap!!.addBundle(hibernate)
        bootstrap.addBundle(object : SwaggerBundle<StoreServiceConfiguration>() {
            override fun getSwaggerBundleConfiguration(configuration: StoreServiceConfiguration): SwaggerBundleConfiguration {
                return configuration.swagger!!
            }
        })
        
        bootstrap.configurationSourceProvider = SubstitutingSourceProvider(
            bootstrap.configurationSourceProvider,
            EnvironmentVariableSubstitutor(false))

        bootstrap.addBundle(object : MigrationsBundle<StoreServiceConfiguration>() {
            override fun getDataSourceFactory(configuration: StoreServiceConfiguration): DataSourceFactory {
                return configuration.database
            }
        })
    }

    override fun run(configuration: StoreServiceConfiguration,
                     environment: Environment) {
        val cors = environment.servlets().addFilter("CORS", CrossOriginFilter::class.java)
        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*")
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin")
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD")
        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType::class.java), true, "/*")

        registerDao(configuration, environment)
        val resources: ImmutableList<Class<*>> = getResources(configuration.resourcePackage)
        resources.forEach(
                action = {
                    environment.jersey().register(it.getConstructor().newInstance())
                }
        )
    }

    private fun registerDao(configuration: StoreServiceConfiguration,
                            environment: Environment) {
        val resources: ImmutableList<Class<*>> = Util.getClassDao(configuration.daoPackage)!!
        resources.forEach(
                action = {
                    val instance = it.getConstructor(SessionFactory::class.java).newInstance(hibernate.sessionFactory)
                    environment.jersey().register(it.getMethod("getInstance", SessionFactory::class.java).invoke(instance, hibernate.sessionFactory))
                }
        )
    }

    fun getEntities(): ImmutableList<Class<*>> {
        return Util.getClassEntities("fr.gbear.webstatgestion.store.db.entity") ?: ImmutableList.copyOf(arrayListOf())
    }

    private fun getResources(packageName: String): ImmutableList<Class<*>> {
        return Util.getClassResources(packageName) ?: ImmutableList.copyOf(arrayListOf())
    }


}