package fr.gbear.webstatgestion.store.db.mapper

import fr.gbear.webstatgestion.store.db.dto.EquipmentBookingDto
import fr.gbear.webstatgestion.store.db.dto.EquipmentDto
import fr.gbear.webstatgestion.store.db.dto.EquipmentEditDto
import fr.gbear.webstatgestion.store.db.dto.ModelDto
import fr.gbear.webstatgestion.store.db.entity.BookingEquipment
import fr.gbear.webstatgestion.store.db.entity.Equipment
import fr.gbear.webstatgestion.store.db.entity.Model
import fr.gbear.webstatgestion.store.util.Util
import java.util.*

class EquipmentMapper {
    companion object {
        private var instance: EquipmentMapper? = null

        @JvmStatic
        fun getInstance(): EquipmentMapper {
            instance.takeIf { instance == null }.let { instance = EquipmentMapper() }
            return instance!!
        }
    }

    val modelMapper = ModelMapper.getInstance()

    fun convertEquipmentDtoToEquipment(equipmentEditDto: EquipmentEditDto): Equipment {
        val equipment = Equipment()
        equipment.size = equipmentEditDto.size
        equipment.shoeSize = equipmentEditDto.shoeSize
        equipment.model = modelMapper.modelDtoToModel(equipmentEditDto.modelEditDto!!)
        equipment.idEquipment = equipmentEditDto.idEquipment
        equipment.price = equipmentEditDto.price
        return equipment
    }

    fun convertEquipmentToEquipmentDto(equipment: Equipment): EquipmentDto {
        val equipmentDto = EquipmentDto()
        equipmentDto.size = equipment.size
        equipmentDto.shoeSize = equipment.shoeSize
        equipmentDto.idModel = equipment.model!!.idModel
        equipmentDto.idEquipment = equipment.idEquipment
        equipmentDto.price = equipment.price
        val bookingsFilters = equipment.bookings!!.filter { booking -> checkDateBooking(booking) }
        equipmentDto.isBooking = bookingsFilters.isNotEmpty()
        bookingsFilters.takeIf { bookingsFilters.isNotEmpty() }?.let { equipmentDto.renderDate = bookingsFilters[0].endDate }
        return equipmentDto
    }

    fun convertEquipmentToEquipmentBookingDto(equipment: Equipment): EquipmentBookingDto {
        val equipmentDto = EquipmentBookingDto()
        equipmentDto.size = equipment.size
        equipmentDto.shoeSize = equipment.shoeSize
        equipmentDto.model = equipment.model
        equipmentDto.idEquipment = equipment.idEquipment
        equipmentDto.price = equipment.price
        return equipmentDto
    }

    /**
     * @param equipments
     * @param model
     *
     * Allows to merge model and equipments object for the front-end
     */
    fun formattedEquipmentRender(model: Model, equipments: List<Equipment>): ModelDto {
        val modelDto = modelMapper.modelToModelDto(model)
        equipments.forEach { equipment: Equipment -> modelDto.equipments.add(convertEquipmentToEquipmentDto(equipment)) }
        val bookingSize = modelDto.equipments.filter { equipmentDto -> equipmentDto.isBooking }.size
        modelDto.stock = modelDto.equipments.size - bookingSize
        return modelDto
    }

    fun checkDateBooking(equipmentBooking: BookingEquipment): Boolean {
        val date = Util.formatterDate(Date())
        return (
                date == equipmentBooking.startDate || date == equipmentBooking.endDate || (date.after(equipmentBooking.startDate) && date.before(equipmentBooking.endDate))
                )
    }
}