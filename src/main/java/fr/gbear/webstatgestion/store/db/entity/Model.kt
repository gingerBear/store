package fr.gbear.webstatgestion.store.db.entity

import com.fasterxml.jackson.annotation.JsonProperty
import fr.gbear.webstatgestion.store.util.annotation.EntityBdd
import org.hibernate.validator.constraints.NotBlank
import javax.persistence.*
import javax.validation.constraints.NotNull

@EntityBdd
@Entity
class Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    @Column(name = "\"idModel\"")
    var idModel: Long? = null

    @Column(name = "\"reference\"", length = 255)
    @NotBlank
    @NotNull
    @JsonProperty
    var reference: String = ""

    @Column(name = "\"model\"", length = 255)
    @NotBlank
    @NotNull
    @JsonProperty
    var model: String = ""

    @Column(name = "\"barcode\"", length = 255)
    @NotNull
    @JsonProperty
    var barcode: Long? = null

    @Column(name = "\"brand\"", length = 255)
    @NotBlank
    @NotNull
    @JsonProperty
    var brand: String = ""

    @Column(name = "\"description\"", length = 1000)
    @NotBlank
    @NotNull
    @JsonProperty
    var description: String = ""

    @Column(name = "\"imagePath\"", length = 255)
    @JsonProperty
    var imagePath: String? = null

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonProperty
    @JoinColumn(name = "\"idStore\"", nullable = false)
    var store: Store? = null
}