package fr.gbear.webstatgestion.store.db.enumeration

enum class StateEquipment(val state: String) {
    NEW("new"),
    USED("used"),
    DAMAGED("damaged")
}