package fr.gbear.webstatgestion.store.db.entity

import com.fasterxml.jackson.annotation.JsonProperty
import fr.gbear.webstatgestion.store.util.annotation.EntityBdd
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@EntityBdd
class Equipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    @Column(name = "\"idEquipment\"", nullable = false)
    var idEquipment: Long? = null

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonProperty
    @JoinColumn(name = "\"idModel\"", nullable = false)
    var model: Model? = null

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonProperty
    @JoinColumn(name = "\"idStore\"", nullable = false)
    var store: Store? = null

    @Column(name = "\"shoeSize\"")
    @JsonProperty
    var shoeSize: Int? = null

    @Column(name = "\"size\"", length = 10)
    @JsonProperty
    var size: String? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "\"idPrice\"")
    @NotNull
    @JsonProperty
    var price: Price? = null

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "idEquipment", cascade = [CascadeType.ALL])
    @NotNull
    @JsonProperty
    var bookings: MutableList<BookingEquipment>? = mutableListOf()

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "idEquipment", cascade = [CascadeType.ALL])
    var statusEquipments: MutableList<StatusEquipment>? = mutableListOf()
}