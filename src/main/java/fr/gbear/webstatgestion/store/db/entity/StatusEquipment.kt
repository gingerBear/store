package fr.gbear.webstatgestion.store.db.entity

import com.fasterxml.jackson.annotation.JsonProperty
import fr.gbear.webstatgestion.store.db.enumeration.StateEquipment
import fr.gbear.webstatgestion.store.util.annotation.EntityBdd
import java.io.Serializable
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

@EntityBdd
@Entity
@Table(name = "\"status_equipment\"")
class StatusEquipment : Serializable {

    @JsonProperty
    @NotNull
    @Id
    @Column(name = "\"idEquipment\"")
    var idEquipment: Long? = null

    @NotNull
    @JsonProperty
    @Column(name = "\"date\"", unique = true)
    @Id
    var date: Date? = null

    @NotNull
    @JsonProperty
    @Column(name = "\"status\"", unique = true)
    @Enumerated(EnumType.STRING)
    var status: StateEquipment? = null
}