package fr.gbear.webstatgestion.store.db.dto

import com.fasterxml.jackson.annotation.JsonProperty
import fr.gbear.webstatgestion.store.db.entity.Insurance
import fr.gbear.webstatgestion.store.db.entity.Model
import fr.gbear.webstatgestion.store.db.entity.Price
import java.util.*
import javax.validation.constraints.NotNull
import kotlin.collections.HashSet

class EquipmentBookingDto {
    @JsonProperty
    var model: Model? = null

    @JsonProperty
    @NotNull
    var price: Price? = null

    @JsonProperty
    var shoeSize: Int? = null

    @JsonProperty
    var size: String? = null

    @JsonProperty
    var idEquipment: Long? = null

    @NotNull
    @JsonProperty
    lateinit var beginDate: Date

    @NotNull
    @JsonProperty
    lateinit var endDate: Date

    @JsonProperty
    var insurancesSubscribes: Set<Insurance> = HashSet()
}