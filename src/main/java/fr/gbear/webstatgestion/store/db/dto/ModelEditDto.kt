package fr.gbear.webstatgestion.store.db.dto

import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.validator.constraints.NotBlank
import javax.validation.constraints.NotNull

class ModelEditDto {
    @JsonProperty
    var idModel: Long? = null

    @NotBlank
    @NotNull
    @JsonProperty
    var reference: String = ""

    @NotBlank
    @NotNull
    @JsonProperty
    var model: String = ""

    @NotNull
    @JsonProperty
    var barcode: Long? = null

    @NotBlank
    @NotNull
    @JsonProperty
    var brand: String = ""

    @NotBlank
    @NotNull
    @JsonProperty
    var description: String = ""

    @NotBlank
    @JsonProperty
    var imagePath: String? = null
}