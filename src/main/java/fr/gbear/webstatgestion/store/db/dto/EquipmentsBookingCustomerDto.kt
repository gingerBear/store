package fr.gbear.webstatgestion.store.db.dto

import com.fasterxml.jackson.annotation.JsonProperty

class EquipmentsBookingCustomerDto {
    @JsonProperty
    var equipments: List<EquipmentBookingDto> = mutableListOf()
    @JsonProperty
    var idCustomer: Long? = null
    @JsonProperty
    var idBooking: Long? = null
}