package fr.gbear.webstatgestion.store.db.dao

import fr.gbear.webstatgestion.store.db.entity.Insurance
import fr.gbear.webstatgestion.store.util.annotation.EntityDao
import io.dropwizard.hibernate.AbstractDAO
import org.hibernate.SessionFactory
import java.util.*

@EntityDao
class InsuranceDao(sessionFactory: SessionFactory?) : AbstractDAO<Insurance>(sessionFactory) {
    companion object {
        private var instance: InsuranceDao? = null

        @JvmStatic
        fun getInstance(sessionFactory: SessionFactory? = null): InsuranceDao {
            if (instance == null || sessionFactory != null) {
                instance = InsuranceDao(sessionFactory)
            }
            return instance!!
        }
    }

    fun findById(id: Long): Optional<Insurance> {
        return try {
            Optional.of(currentSession().get(Insurance::class.java, id) as Insurance)
        } catch (e: Exception) {
            Optional.empty()
        }
    }

    fun findAllInsurances(idStore: Long): List<Insurance> {
        val query = query("select e from Insurance e where e.idStore = :idStore")
        query.setParameter("idStore", idStore)
        return list(query)
    }


    fun update(insurance: Insurance): Optional<Insurance> {
        currentSession().update(insurance)
        return Optional.of(insurance)
    }

    fun insert(insurance: Insurance): Optional<Insurance> {
        return Optional.of(persist(insurance))
    }

    fun delete(insuranceId: Long): Optional<Insurance> {
        val insuranceOpt = findById(insuranceId)
        insuranceOpt.ifPresent { t: Insurance -> t.isActive = false; update(t) }
        return insuranceOpt
    }
}
