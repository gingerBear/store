package fr.gbear.webstatgestion.store.db.dao

import fr.gbear.webstatgestion.store.db.entity.Booking
import fr.gbear.webstatgestion.store.util.annotation.EntityDao
import io.dropwizard.hibernate.AbstractDAO
import org.hibernate.SessionFactory
import java.util.*

@EntityDao
class BookingDao(sessionFactory: SessionFactory?) : AbstractDAO<Booking>(sessionFactory) {
    companion object {
        private var instance: BookingDao? = null

        @JvmStatic
        fun getInstance(sessionFactory: SessionFactory? = null): BookingDao {
            if (instance == null || sessionFactory != null) {
                instance = BookingDao(sessionFactory)
            }
            return instance!!
        }
    }

    fun findById(id: Long): Optional<Booking> {
        return try {
            Optional.of(currentSession().get(Booking::class.java, id) as Booking)
        } catch (e: Exception) {
            Optional.empty()
        }
    }

    fun delete(booking: Booking) {
        currentSession().delete(booking)
    }

    fun update(booking: Booking): Optional<Booking> {
        currentSession().saveOrUpdate(booking)
        return Optional.of(booking)
    }

    fun insert(booking: Booking): Optional<Booking> {
        return Optional.of(persist(booking))
    }

    fun findAllBooking(): List<Booking> {
        return list(query("select p from Booking p"))
    }

    fun findAllBookingByCustomer(idCustomer: Long): List<Booking> {
        val query = query("select p from Booking p where p.idCustomer = :idCustomer")
        query.setParameter("idCustomer", idCustomer)
        return query.resultList
    }

    fun findAllBookingByCustomerAndStore(idStore: Long, idCustomer: Long?): List<Booking> {
        val queryWithStore = "select p from Booking p where p.store.idStore = :idStore"
        var query = query(queryWithStore)
        if (idCustomer != null && idCustomer > 0) {
            query = query("select p from Booking p where p.idCustomer = :idCustomer and p.store.idStore = :idStore")
            query.setParameter("idCustomer", idCustomer)
        }
        query.setParameter("idStore", idStore)
        return query.resultList
    }
}