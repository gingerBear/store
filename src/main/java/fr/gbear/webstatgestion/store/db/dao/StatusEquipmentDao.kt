package fr.gbear.webstatgestion.store.db.dao

import fr.gbear.webstatgestion.store.db.entity.StatusEquipment
import fr.gbear.webstatgestion.store.util.annotation.EntityDao
import io.dropwizard.hibernate.AbstractDAO
import org.hibernate.SessionFactory
import java.util.*

@EntityDao
class StatusEquipmentDao(sessionFactory: SessionFactory?) : AbstractDAO<StatusEquipment>(sessionFactory) {
    companion object {
        private var instance: StatusEquipmentDao? = null

        @JvmStatic
        fun getInstance(sessionFactory: SessionFactory? = null): StatusEquipmentDao {
            if (instance == null || sessionFactory != null) {
                instance = StatusEquipmentDao(sessionFactory)
            }
            return instance!!
        }
    }

    fun insert(statusEquipment: StatusEquipment): Optional<StatusEquipment> {
        return Optional.of(persist(statusEquipment))
    }

    fun findStatus(idEquipment: Long): List<StatusEquipment> {
        val query = query("select s from StatusEquiment s where s = :idEquipment order by s.date")
        query.setParameter("idEquipment", idEquipment)
        return query.resultList
    }
}