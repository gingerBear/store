package fr.gbear.webstatgestion.store.db.mapper

import fr.gbear.webstatgestion.store.db.dto.ModelDto
import fr.gbear.webstatgestion.store.db.dto.ModelEditDto
import fr.gbear.webstatgestion.store.db.entity.Model

class ModelMapper {

    companion object {
        private var instance: ModelMapper? = null

        @JvmStatic
        fun getInstance(): ModelMapper {
            instance.takeIf { instance == null }.let { instance = ModelMapper() }
            return instance!!
        }
    }

    fun modelDtoToModel(modelEditDto: ModelEditDto): Model {
        val model = Model()
        model.idModel = modelEditDto.idModel
        model.barcode = modelEditDto.barcode
        model.brand = modelEditDto.brand
        model.description = modelEditDto.description
        model.reference = modelEditDto.reference
        model.model = modelEditDto.model
        model.imagePath = modelEditDto.imagePath
        return model
    }

    fun modelToModelDto(model: Model): ModelDto {
        val modelDto = ModelDto()
        modelDto.idModel = model.idModel
        modelDto.barcode = model.barcode
        modelDto.brand = model.brand
        modelDto.description = model.description
        modelDto.reference = model.reference
        modelDto.model = model.model
        modelDto.imagePath = model.imagePath
        return modelDto
    }
}