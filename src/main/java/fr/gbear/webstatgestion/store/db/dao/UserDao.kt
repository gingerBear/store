package fr.gbear.webstatgestion.store.db.dao

import fr.gbear.webstatgestion.store.db.entity.User
import fr.gbear.webstatgestion.store.util.annotation.EntityDao
import io.dropwizard.hibernate.AbstractDAO
import org.hibernate.SessionFactory
import java.util.*

@EntityDao
class UserDao constructor(sessionFactory: SessionFactory?) : AbstractDAO<User>(sessionFactory) {

    companion object {
        private var instance: UserDao? = null

        @JvmStatic
        fun getInstance(sessionFactory: SessionFactory? = null): UserDao {
            if (instance == null || sessionFactory != null) {
                instance = UserDao(sessionFactory)
            }
            return instance!!
        }
    }


    fun findById(id: Long): Optional<User> {
        return try {
            Optional.of(currentSession().get(User::class.java, id) as User)
        } catch (e: Exception) {
            Optional.empty()
        }
    }

    fun findByEmail(email: String): Optional<User> {
        return try {
            val query = query("select u from User u where u.email = :email ")
            query.setParameter("email", email)
            return Optional.of(query.singleResult)
        } catch (e: Exception) {
            Optional.empty()
        }
    }

    fun delete(user: User) {
        currentSession().delete(user)
    }

    fun update(user: User): Optional<User> {
        currentSession().saveOrUpdate(user)
        return Optional.of(user)
    }

    fun insert(user: User): Optional<User> {
        return Optional.of(persist(user))
    }

    fun findAllUser(): List<User> {
        return list(query("select u from User u"))
    }
}