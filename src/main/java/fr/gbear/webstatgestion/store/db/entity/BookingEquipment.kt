package fr.gbear.webstatgestion.store.db.entity

import com.fasterxml.jackson.annotation.JsonProperty
import fr.gbear.webstatgestion.store.util.annotation.EntityBdd
import java.io.Serializable
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull


@EntityBdd
@Entity
@Table(name = "booking_equipment")
class BookingEquipment : Serializable {
    @Id
    @JsonProperty
    @NotNull
    @Column(name = "\"idEquipment\"")
    var idEquipment: Long? = null

    @Column(name = "\"idBooking\"")
    @NotNull
    @JsonProperty
    @Id
    var idBooking: Long? = null

    @Column(name = "\"startDate\"")
    @NotNull
    @JsonProperty
    var startDate: Date? = null

    @Column(name = "\"endDate\"", length = 255)
    @NotNull
    @JsonProperty
    var endDate: Date? = null

    @ManyToMany(cascade = [CascadeType.ALL])
    @JoinTable(name = "subscribe_insurance", inverseJoinColumns = [JoinColumn(name = "\"idInsurance\"")],
            joinColumns = [JoinColumn(name = "\"idEquipment\""), JoinColumn(name = "\"idBooking\"")])
    var insurance: Set<Insurance> = HashSet<Insurance>()


}