package fr.gbear.webstatgestion.store.db.dto

import com.fasterxml.jackson.annotation.JsonProperty
import fr.gbear.webstatgestion.store.db.entity.Price
import javax.validation.constraints.NotNull

class EquipmentEditDto {
    @JsonProperty
    var idEquipment: Long? = null

    @JsonProperty
    @NotNull
    var modelEditDto: ModelEditDto? = null

    @JsonProperty
    @NotNull
    var price: Price? = null

    @JsonProperty
    var shoeSize: Int? = null

    @JsonProperty
    var size: String? = null
}