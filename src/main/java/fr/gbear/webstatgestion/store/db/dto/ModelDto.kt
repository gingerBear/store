package fr.gbear.webstatgestion.store.db.dto

import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.validator.constraints.NotBlank
import java.util.*
import javax.validation.constraints.NotNull

class ModelDto {
    @JsonProperty
    var idModel: Long? = null

    @NotBlank
    @NotNull
    @JsonProperty
    var reference: String = ""

    @NotBlank
    @NotNull
    @JsonProperty
    var model: String = ""

    @NotNull
    @JsonProperty
    var barcode: Long? = null

    @NotBlank
    @NotNull
    @JsonProperty
    var brand: String = ""

    @NotBlank
    @NotNull
    @JsonProperty
    var description: String = ""

    @NotNull
    @JsonProperty
    var equipments: MutableList<EquipmentDto> = LinkedList()

    @NotNull
    @JsonProperty
    var stock: Int = 0

    @NotBlank
    @JsonProperty
    var imagePath: String? = null


}