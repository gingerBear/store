package fr.gbear.webstatgestion.store.db.entity

import com.fasterxml.jackson.annotation.JsonProperty
import fr.gbear.webstatgestion.store.util.annotation.EntityBdd
import javax.persistence.*
import javax.validation.constraints.NotNull

@EntityBdd
@Entity
class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    @Column(name = "\"idBooking\"")
    var idBooking: Long? = null

    @Column(name = "\"idCustomer\"", length = 255)
    @NotNull
    @JsonProperty
    var idCustomer: Long? = null

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "idBooking", cascade = [CascadeType.ALL])
    @NotNull
    @JsonProperty
    var bookingsEquipments: MutableList<BookingEquipment> = mutableListOf()

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonProperty
    @JoinColumn(name = "\"idStore\"", nullable = false)
    var store: Store? = null


    /*@OneToMany(fetch = FetchType.LAZY, mappedBy = "idBooking", cascade = [CascadeType.ALL])
    @NotNull
    @JsonProperty
    var subscribeInsurance: MutableList<SubscribeInsurance> = mutableListOf()*/
}