package fr.gbear.webstatgestion.store.db.entity

import com.fasterxml.jackson.annotation.JsonProperty
import fr.gbear.webstatgestion.store.util.annotation.EntityBdd

import javax.persistence.*
import javax.validation.constraints.NotNull

@EntityBdd
@Entity
class Insurance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    @Column(name = "\"idInsurance\"", nullable = false)
    var idInsurance: Long? = null

    @JsonProperty
    @Column(name = "\"name\"", nullable = false)
    @NotNull
    var name: String? = null

    @Column(name = "\"insuranceRate\"")
    @JsonProperty
    var insuranceRate: Double? = null

    @Column(name = "\"price\"")
    @JsonProperty
    var price: Double? = null

    @Column(name = "\"isActive\"")
    @JsonProperty
    var isActive: Boolean? = true

    @NotNull
    @JsonProperty
    @Column(name = "\"idStore\"", nullable = false)
    var idStore: Long? = null
}
