package fr.gbear.webstatgestion.store.db.dao

import fr.gbear.webstatgestion.store.db.entity.Model
import fr.gbear.webstatgestion.store.util.annotation.EntityDao
import io.dropwizard.hibernate.AbstractDAO
import org.hibernate.SessionFactory
import java.util.*

@EntityDao
class ModelDao(sessionFactory: SessionFactory?) : AbstractDAO<Model>(sessionFactory) {
    companion object {
        private var instance: ModelDao? = null

        @JvmStatic
        fun getInstance(sessionFactory: SessionFactory? = null): ModelDao {
            if (instance == null || sessionFactory != null) {
                instance = ModelDao(sessionFactory)
            }
            return instance!!
        }
    }

    fun findById(id: Long): Optional<Model> {
        return try {
            Optional.of(currentSession().get(Model::class.java, id) as Model)
        } catch (e: Exception) {
            Optional.empty()
        }
    }

    fun delete(model: Model) {
        currentSession().delete(model)
    }

    fun update(model: Model): Optional<Model> {
        currentSession().saveOrUpdate(model)
        return Optional.of(model)
    }

    fun insert(model: Model): Optional<Model> {
        return Optional.of(persist(model))
    }

    fun findAllModel(id: Long): List<Model> {
        val query = query("select m from Model m where m.store.idStore = :id")
        query.setParameter("id", id)
        return query.list()
    }
}