package fr.gbear.webstatgestion.store.db.entity

import com.fasterxml.jackson.annotation.JsonProperty
import fr.gbear.webstatgestion.store.util.annotation.EntityBdd
import org.hibernate.validator.constraints.NotBlank
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

@EntityBdd
@Entity
@Table(name = "\"user\"")
class User {

    @Column(name = "\"idUser\"")
    @JsonProperty
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var idUser: Long? = null

    @Column(name = "\"lastName\"", nullable = false)
    @JsonProperty
    var lastName: String = ""

    @Column(name = "\"firstName\"", nullable = false)
    @JsonProperty
    var firstName: String = ""

    @Column(name = "\"arrivalDate\"")
    @JsonProperty
    var arrivalDate: Date? = null

    @Column(name = "\"departureDate\"")
    @JsonProperty
    var departureDate: Date? = null

    @Column(name = "\"isAdmin\"", nullable = false)
    @JsonProperty
    var isAdmin: Boolean = false

    @Column(name = "\"email\"", nullable = false, length = 255)
    @NotNull
    @NotBlank
    @JsonProperty
    var email: String = ""

    @Column(name = "\"idStore\"")
    @JsonProperty
    var idStore: Long? = null
}