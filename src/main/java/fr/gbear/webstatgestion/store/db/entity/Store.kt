package fr.gbear.webstatgestion.store.db.entity

import com.fasterxml.jackson.annotation.JsonProperty
import fr.gbear.webstatgestion.store.util.annotation.EntityBdd
import org.hibernate.validator.constraints.NotBlank
import javax.persistence.*
import javax.validation.constraints.NotNull

@EntityBdd
@Entity
class Store {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    @Column(name = "\"idStore\"")
    var idStore: Long? = null

    @Column(name = "\"name\"", length = 255)
    @NotBlank
    @NotNull
    @JsonProperty
    var name: String = ""

    @Column(name = "\"address\"", length = 255)
    @NotBlank
    @NotNull
    @JsonProperty
    var address: String = ""

    @Column(name = "\"cp\"", length = 10)
    @NotBlank
    @NotNull
    @JsonProperty
    var cp: String = ""

    @Column(name = "\"city\"", length = 150)
    @NotBlank
    @NotNull
    @JsonProperty
    var city: String = ""

    @Column(name = "\"latitude\"")
    @JsonProperty
    var latitude: Double? = null

    @Column(name = "\"longitude\"")
    @JsonProperty
    var longitude: Double? = null

}