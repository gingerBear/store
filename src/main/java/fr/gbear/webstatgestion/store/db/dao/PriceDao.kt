package fr.gbear.webstatgestion.store.db.dao

import fr.gbear.webstatgestion.store.db.entity.Price
import fr.gbear.webstatgestion.store.util.annotation.EntityDao
import io.dropwizard.hibernate.AbstractDAO
import org.hibernate.SessionFactory
import java.util.*

@EntityDao
class PriceDao(sessionFactory: SessionFactory?) : AbstractDAO<Price>(sessionFactory) {
    companion object {
        private var instance: PriceDao? = null

        @JvmStatic
        fun getInstance(sessionFactory: SessionFactory? = null): PriceDao {
            if (instance == null || sessionFactory != null) {
                instance = PriceDao(sessionFactory)
            }
            return instance!!
        }
    }

    fun findById(id: Long): Optional<Price> {
        return try {
            Optional.of(currentSession().get(Price::class.java, id) as Price)
        } catch (e: Exception) {
            Optional.empty()
        }
    }

    fun delete(price: Price) {
        currentSession().delete(price)
    }

    fun update(price: Price): Optional<Price> {
        currentSession().saveOrUpdate(price)
        return Optional.of(price)
    }

    fun insert(price: Price): Optional<Price> {
        return Optional.of(persist(price))
    }

    fun findAllPrice(): List<Price> {
        return list(query("select p from Price p"))
    }
}