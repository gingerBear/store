package fr.gbear.webstatgestion.store.db.dao

import fr.gbear.webstatgestion.store.db.entity.Equipment
import fr.gbear.webstatgestion.store.util.annotation.EntityDao
import io.dropwizard.hibernate.AbstractDAO
import org.hibernate.SessionFactory
import java.util.*

@EntityDao
class EquipmentDao(sessionFactory: SessionFactory?) : AbstractDAO<Equipment>(sessionFactory) {
    companion object {
        private var instance: EquipmentDao? = null

        @JvmStatic
        fun getInstance(sessionFactory: SessionFactory? = null): EquipmentDao {
            if (instance == null || sessionFactory != null) {
                instance = EquipmentDao(sessionFactory)
            }
            return instance!!
        }
    }

    fun findById(idStore: Long? = null, id: Long): Optional<Equipment> {
        val queryStringWithStore = "Select e from Equipment e where e.store.idStore = :idStore and e.idEquipment = :id "
        val queryString = "Select e from Equipment e where e.idEquipment = :id "
        return try {
            var query = query(queryString)
            if (idStore != null) {
                query = query(queryStringWithStore)
                query.setParameter("idStore", idStore)
            }
            query.setParameter("id", id)
            Optional.of(query.uniqueResult() as Equipment)
        } catch (e: Exception) {
            Optional.empty()
        }
    }


    fun update(equipment: Equipment): Optional<Equipment> {
        currentSession().saveOrUpdate(equipment)
        return Optional.of(equipment)
    }

    fun insert(equipment: Equipment): Optional<Equipment> {
        return Optional.of(persist(equipment))
    }

    fun findAllEquipment(idStore: Long): List<Equipment> {
        val query = query("select e from Equipment e where e.store.idStore = :idStore")
        query.setParameter("idStore", idStore)
        return list(query)
    }

    fun findAllEquipmentByModel(idStore: Long, idModel: Long): List<Equipment> {
        val query = query("select e from Equipment e where e.store.idStore = :idStore and e.model.idModel = :idModel")
        query.setParameter("idStore", idStore)
        query.setParameter("idModel", idModel)
        return list(query)
    }

    fun deleteEquipment(equipment: Equipment): Boolean {
        currentSession().delete(equipment)
        return true
    }


}