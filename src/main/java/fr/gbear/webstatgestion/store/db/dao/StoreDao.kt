package fr.gbear.webstatgestion.store.db.dao

import fr.gbear.webstatgestion.store.db.entity.Store
import fr.gbear.webstatgestion.store.util.annotation.EntityDao
import io.dropwizard.hibernate.AbstractDAO
import org.hibernate.SessionFactory
import java.util.*

@EntityDao
class StoreDao constructor(sessionFactory: SessionFactory?) : AbstractDAO<Store>(sessionFactory) {
    companion object {
        private var instance: StoreDao? = null

        @JvmStatic
        fun getInstance(sessionFactory: SessionFactory? = null): StoreDao {
            if (instance == null || sessionFactory != null) {
                instance = StoreDao(sessionFactory)
            }
            return instance!!
        }
    }


    fun findById(id: Long): Optional<Store> {
        return try {
            Optional.of(currentSession().get(Store::class.java, id) as Store)
        } catch (e: Exception) {
            Optional.empty()
        }
    }

    fun delete(store: Store) {
        currentSession().delete(store)
    }

    fun update(store: Store): Optional<Store> {
        currentSession().saveOrUpdate(store)
        return Optional.of(store)
    }

    fun insert(store: Store): Optional<Store> {
        return Optional.of(persist(store))
    }

    fun findAllStore(): List<Store> {
        return list(query("select s from Store s"))
    }
}