package fr.gbear.webstatgestion.store.db.entity

import com.fasterxml.jackson.annotation.JsonProperty
import fr.gbear.webstatgestion.store.util.annotation.EntityBdd
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@EntityBdd
class Price {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    @Column(name = "\"idPrice\"")
    var idPrice: Long? = null

    @Column(name = "\"price\"", precision = 2)
    @NotNull
    @JsonProperty
    var price: Double? = null

    @Column(name = "\"date\"")
    @JsonProperty
    var date: Date? = null
}