package fr.gbear.webstatgestion.store.db.dao

import fr.gbear.webstatgestion.store.db.entity.BookingEquipment
import fr.gbear.webstatgestion.store.util.annotation.EntityDao
import io.dropwizard.hibernate.AbstractDAO
import org.hibernate.SessionFactory
import java.util.*

@EntityDao
class BookingEquipmentDao(sessionFactory: SessionFactory?) : AbstractDAO<BookingEquipment>(sessionFactory) {
    companion object {
        private var instance: BookingEquipmentDao? = null

        @JvmStatic
        fun getInstance(sessionFactory: SessionFactory? = null): BookingEquipmentDao {
            if (instance == null || sessionFactory != null) {
                instance = BookingEquipmentDao(sessionFactory)
            }
            return instance!!
        }
    }

    fun findById(id: Long): Optional<BookingEquipment> {
        return try {
            Optional.of(currentSession().get(BookingEquipment::class.java, id) as BookingEquipment)
        } catch (e: Exception) {
            Optional.empty()
        }
    }

    fun delete(booking: BookingEquipment) {
        currentSession().delete(booking)
    }

    fun update(booking: BookingEquipment): Optional<BookingEquipment> {
        currentSession().saveOrUpdate(booking)
        return Optional.of(booking)
    }

    fun insert(booking: BookingEquipment): Optional<BookingEquipment> {
        return Optional.of(persist(booking))
    }

    fun findAllBookingEquipment(): List<BookingEquipment> {
        return list(query("select p from BookingEquipment p"))
    }
}