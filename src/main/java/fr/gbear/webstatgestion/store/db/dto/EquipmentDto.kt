package fr.gbear.webstatgestion.store.db.dto

import com.fasterxml.jackson.annotation.JsonProperty
import fr.gbear.webstatgestion.store.db.entity.Price
import java.util.*
import javax.validation.constraints.NotNull

class EquipmentDto {
    @JsonProperty
    var idEquipment: Long? = null

    @JsonProperty
    @NotNull
    var idModel: Long? = null

    @JsonProperty
    @NotNull
    var price: Price? = null

    @JsonProperty
    var shoeSize: Int? = null

    @JsonProperty
    var size: String? = null

    @JsonProperty
    @NotNull
    var isBooking = false

    @JsonProperty
    @NotNull
    var renderDate: Date? = null
}