package fr.gbear.webstatgestion.store.db.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*
import javax.validation.constraints.NotNull
import kotlin.collections.HashSet

class EquipmentBookingEditDto {
    @JsonProperty
    var idEquipment: Long? = null

    @NotNull
    @JsonProperty
    lateinit var beginDate: Date

    @NotNull
    @JsonProperty
    lateinit var endDate: Date

    @NotNull
    @JsonProperty
    var idCustomer: Long? = null

    @JsonProperty
    var insurancesSubscribes: HashSet<Long> = HashSet()

}
