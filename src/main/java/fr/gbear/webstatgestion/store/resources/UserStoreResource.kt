package fr.gbear.webstatgestion.store.resources

import com.codahale.metrics.annotation.Timed
import com.google.gson.Gson
import fr.gbear.webstatgestion.store.db.entity.User
import fr.gbear.webstatgestion.store.service.UserService
import fr.gbear.webstatgestion.store.service.impl.UserServiceImpl
import fr.gbear.webstatgestion.store.util.annotation.ResourcesRest
import fr.gbear.webstatgestion.store.util.exception.user.UserFindException
import io.dropwizard.hibernate.UnitOfWork
import io.swagger.annotations.*
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/stores/{idStore}/users")
@Api(value = "/user", description = "Allows to manage a user (admin or normal)")
@SwaggerDefinition
@ResourcesRest
class UserStoreResource {

    private var userService: UserService? = UserServiceImpl()
    private val gson = Gson()

    @POST
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Create an user")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun createUser(@PathParam("idStore") idStore: Long, user: User): Response {
        val newUser = userService?.createUser(user)
        return Response.ok(gson.toJson(newUser), MediaType.APPLICATION_JSON).build()
    }


    @PUT
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Update an user")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun updateUser(@PathParam("idStore") idStore: Long, user: User): Response {
        return try {
            val newUser = userService?.updateUser(user)
            Response.ok(gson.toJson(newUser), MediaType.APPLICATION_JSON).build()
        } catch (e: UserFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        }
    }

    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get users")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun getUsers(@PathParam("idStore") idStore: Long): Response {
        return Response.ok(gson.toJson(userService?.getUsers()), MediaType.APPLICATION_JSON).build()
    }
}

