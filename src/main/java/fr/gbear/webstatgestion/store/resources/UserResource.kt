package fr.gbear.webstatgestion.store.resources

import com.codahale.metrics.annotation.Timed
import com.google.gson.Gson
import fr.gbear.webstatgestion.store.service.UserService
import fr.gbear.webstatgestion.store.service.impl.UserServiceImpl
import fr.gbear.webstatgestion.store.util.annotation.ResourcesRest
import fr.gbear.webstatgestion.store.util.exception.user.UserFindException
import io.dropwizard.hibernate.UnitOfWork
import io.swagger.annotations.*
import org.hibernate.validator.constraints.Email
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@ResourcesRest
@Path("/users")
@Api(value = "/users", description = "Allows to manage a equipment")
@SwaggerDefinition
class UserResource {

    private var userService: UserService? = UserServiceImpl()
    private val gson = Gson()

    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get all equpments of a store")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun getUserByEmail(@QueryParam("email") @Email email: String): Response {
        return try {
            Response.ok(gson.toJson(userService?.getUserByEmail(email)), MediaType.APPLICATION_JSON).build()
        } catch (e: UserFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        }
    }
}