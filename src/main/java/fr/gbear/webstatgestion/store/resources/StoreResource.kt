package fr.gbear.webstatgestion.store.resources

import com.codahale.metrics.annotation.Timed
import com.google.gson.Gson
import fr.gbear.webstatgestion.store.db.entity.Store
import fr.gbear.webstatgestion.store.service.StoreService
import fr.gbear.webstatgestion.store.service.impl.StoreServiceImpl
import fr.gbear.webstatgestion.store.util.annotation.ResourcesRest
import fr.gbear.webstatgestion.store.util.exception.store.StoreFindException
import io.dropwizard.hibernate.UnitOfWork
import io.swagger.annotations.*
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/stores")
@Api(value = "/stores", description = "Allows to manage a store")
@SwaggerDefinition
@ResourcesRest
class StoreResource {
    private var storeService: StoreService? = StoreServiceImpl()
    private val gson = Gson()

    @POST
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Create a store")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )

    @UnitOfWork
    fun createStore(store: Store): Response {
        val newStore = storeService?.createStore(store)
        return Response.ok(gson.toJson(newStore), MediaType.APPLICATION_JSON).build()
    }

    @PUT
    @Path("{id}")
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Update a store")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun updateStore(@PathParam("id") id : Long, store: Store): Response {

        return try {
            store.idStore = id
            val storeUpdated = storeService?.updateStore(store)
            Response.ok(gson.toJson(storeUpdated), MediaType.APPLICATION_JSON).build()
        } catch (e: StoreFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        }
    }

    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get store")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun getStores(): Response {
        return Response.ok(gson.toJson(storeService?.getStores()), MediaType.APPLICATION_JSON).build()
    }

    @GET
    @Path("{id}")
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "get a store by his ID")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun getStore(@PathParam("id") id : Long): Response {
        val store = storeService?.getStore(id)
        return Response.ok(gson.toJson(store), MediaType.APPLICATION_JSON).build()
    }
}