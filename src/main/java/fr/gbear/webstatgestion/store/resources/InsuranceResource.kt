package fr.gbear.webstatgestion.store.resources

import com.codahale.metrics.annotation.Timed
import com.google.gson.Gson
import fr.gbear.webstatgestion.store.db.entity.Insurance
import fr.gbear.webstatgestion.store.service.InsuranceService
import fr.gbear.webstatgestion.store.service.impl.InsuranceServiceImpl
import fr.gbear.webstatgestion.store.util.annotation.ResourcesRest
import fr.gbear.webstatgestion.store.util.exception.insurance.InsuranceCreationException
import fr.gbear.webstatgestion.store.util.exception.insurance.InsuranceFindException
import fr.gbear.webstatgestion.store.util.exception.store.StoreFindException
import io.dropwizard.hibernate.UnitOfWork
import io.swagger.annotations.*
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@ResourcesRest
@Path("/stores/{idStore}/insurances")
@Api(value = "//stores/{idStore}/insurances", description = "Allows to manage an insurance")
@SwaggerDefinition
class InsuranceResource {
    private var insuranceService: InsuranceService? = InsuranceServiceImpl()
    private val gson = Gson()

    /**
     *  @param insurance
     *  @param idStore
     *  @return Response
     *
     *  Allows to create an insurance.
     */
    @POST
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Create an insurance")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun createInsurance(@PathParam("idStore") idStore: Long, insurance: Insurance): Response? {
        return try {
            val newInsurance = insuranceService?.createAnInsurance(idStore, insurance)
            Response.ok(gson.toJson(newInsurance), MediaType.APPLICATION_JSON).build()
        } catch (e: Exception) {
            when (e) {
                is InsuranceCreationException, is StoreFindException ->
                    Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
                else -> Response.status(Response.Status.NOT_FOUND).entity("Unknown error : ${e.message}").type(MediaType.TEXT_PLAIN_TYPE).build()
            }
        }
    }

    /**
     *  @param insurance
     *  @param idStore
     *  @return Response
     *
     *  Allows to update an insurance.
     */
    @PUT
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Update an insurance")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    @Path("/{id}")
    fun updateInsurance(@PathParam("idStore") idStore: Long, @PathParam("id") idInsurance: Long, insurance: Insurance): Response? {
        return try {
            insurance.idInsurance = idInsurance
            val newInsurance = insuranceService?.updateAnInsurance(idStore, insurance)
            Response.ok(gson.toJson(newInsurance), MediaType.APPLICATION_JSON).build()
        } catch (e: Exception) {
            when (e) {
                is InsuranceFindException, is StoreFindException ->
                    Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
                else -> Response.status(Response.Status.NOT_FOUND).entity("Unknown error : ${e.message}").type(MediaType.TEXT_PLAIN_TYPE).build()
            }
        }
    }

    /**
     *  @param idInsurance
     *  @param idStore
     *  @return Response
     *
     *  Allows to delete an insurance. (change boolean isActive)
     */
    @DELETE
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Delete an insurance")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    @Path("/{id}")
    fun deleteInsurance(@PathParam("idStore") idStore: Long, @PathParam("id") idInsurance: Long): Response? {
        return try {
            val insurance = insuranceService?.deleteInsurance(idStore, idInsurance)
            Response.ok(gson.toJson(insurance), MediaType.APPLICATION_JSON).build()
        } catch (e: Exception) {
            when (e) {
                is InsuranceFindException, is StoreFindException ->
                    Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
                else -> Response.status(Response.Status.NOT_FOUND).entity("Unknown error : ${e.message}").type(MediaType.TEXT_PLAIN_TYPE).build()
            }
        }
    }

    /**
     *  @param idStore
     *  @return Response
     *
     *  Allows to get all insurances
     */
    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "get insurances")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun getInsurances(@PathParam("idStore") idStore: Long): Response? {
        return try {
            val insurance = insuranceService?.getInsurances(idStore)
            Response.ok(gson.toJson(insurance), MediaType.APPLICATION_JSON).build()
        } catch (e: Exception) {
            when (e) {
                is InsuranceFindException, is StoreFindException ->
                    Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
                else -> Response.status(Response.Status.NOT_FOUND).entity("Unknown error : ${e.message}").type(MediaType.TEXT_PLAIN_TYPE).build()
            }
        }
    }
}