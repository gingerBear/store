package fr.gbear.webstatgestion.store.resources

import com.codahale.metrics.annotation.Timed
import com.google.gson.Gson
import fr.gbear.webstatgestion.store.db.dto.ModelEditDto
import fr.gbear.webstatgestion.store.service.ModelService
import fr.gbear.webstatgestion.store.service.impl.ModelServiceImpl
import fr.gbear.webstatgestion.store.util.annotation.ResourcesRest
import fr.gbear.webstatgestion.store.util.exception.model.ModelFindException
import fr.gbear.webstatgestion.store.util.exception.store.StoreFindException
import io.dropwizard.hibernate.UnitOfWork
import io.swagger.annotations.*
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/stores/{idStore}/models")
@Api(value = "model", description = "Allows to manage a model (admin or normal)")
@SwaggerDefinition
@ResourcesRest
class ModelResource {

    private var modelService: ModelService? = ModelServiceImpl()
    private val gson = Gson()

    @POST
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Create an modelEdit")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun createModel(@PathParam("idStore") idStore: Long, modelEdit: ModelEditDto): Response {
        return try {
            val newModel = modelService!!.createModel(idStore, modelEdit)
            Response.ok(gson.toJson(newModel), MediaType.APPLICATION_JSON).build()
        } catch (e: StoreFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        }
    }


    @PUT
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Update an model")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    @Path("{id}")
    fun updateModel(@PathParam("idStore") idStore: Long, @PathParam("id") id: Long, modelEdit: ModelEditDto): Response {
        return try {
            modelEdit.idModel = id
            val modelUpdated = modelService?.updateModel(idStore, modelEdit)
            Response.ok(gson.toJson(modelUpdated), MediaType.APPLICATION_JSON).build()
        } catch (e: Exception) {
            when (e) {
                is StoreFindException,
                is ModelFindException -> {
                    Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
                }
                else -> {
                    Response.status(500).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
                }
            }
        }
    }

    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get models")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun getModels(@PathParam("idStore") idStore: Long): Response {
        return try {
            Response.ok(gson.toJson(modelService?.getModels(idStore)), MediaType.APPLICATION_JSON).build()
        } catch (e: StoreFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        }
    }
}

