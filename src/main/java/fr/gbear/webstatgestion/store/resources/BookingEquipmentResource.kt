package fr.gbear.webstatgestion.store.resources

import com.codahale.metrics.annotation.Timed
import com.google.gson.Gson
import fr.gbear.webstatgestion.store.db.dto.EquipmentBookingEditDto
import fr.gbear.webstatgestion.store.service.EquipmentService
import fr.gbear.webstatgestion.store.service.impl.EquipmentServiceImpl
import fr.gbear.webstatgestion.store.util.annotation.ResourcesRest
import fr.gbear.webstatgestion.store.util.exception.store.StoreFindException
import io.dropwizard.hibernate.UnitOfWork
import io.swagger.annotations.*
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@ResourcesRest
@Path("/stores/{idStore}/equipment/booking")
@Api(value = "/stores/{idStore}/equipment/booking", description = "Allows to manage a equipment")
@SwaggerDefinition
class BookingEquipmentResource {
    private var equipmentService: EquipmentService? = EquipmentServiceImpl()
    private val gson = Gson()
    /**
     *  @param bookingEditDtos
     *  @param idStore
     *  @return Response
     *
     */
    @POST
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "booking equipment")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun bookingEquipment(@PathParam("idStore") idStore: Long, bookingEditDtos: List<EquipmentBookingEditDto>): Response? {
        return try {
            val equiments = equipmentService?.bookingEquipments(idStore, bookingEditDtos)
            Response.ok(gson.toJson(equiments), MediaType.APPLICATION_JSON).build()
        } catch (e: StoreFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        } catch (e: Exception) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        }
    }

    /**
     *  @param idCustomer
     *  @param idStore
     *  @return Response
     *
     */
    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Allow to get a booking equipment by customer for a store")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun bookingEquipmentByCustomerForAStore(@PathParam("idStore") idStore: Long, @QueryParam("idCustomer") idCustomer: Long): Response? {
        return try {
            val booking = equipmentService?.getBookingEquipmentsCustomerForStore(idStore, idCustomer)
            Response.ok(gson.toJson(booking), MediaType.APPLICATION_JSON).build()
        } catch (e: StoreFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        } catch (e: Exception) {
            Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        }
    }
}