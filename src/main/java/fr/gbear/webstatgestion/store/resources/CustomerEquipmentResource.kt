package fr.gbear.webstatgestion.store.resources

import com.codahale.metrics.annotation.Timed
import com.google.gson.Gson
import fr.gbear.webstatgestion.store.service.CustomerService
import fr.gbear.webstatgestion.store.service.impl.CustomerServiceImpl
import fr.gbear.webstatgestion.store.util.annotation.ResourcesRest
import fr.gbear.webstatgestion.store.util.exception.store.StoreFindException
import io.dropwizard.hibernate.UnitOfWork
import io.swagger.annotations.*
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@ResourcesRest
@Path("/customers/{idCustomers}/bookings/equipments")
@Api(value = "\\customers\\{idCustomers}\\equipments", description = "Allows to get all equipment booking by a customers ")
@SwaggerDefinition
class CustomerEquipmentResource {
    private var customerService: CustomerService? = CustomerServiceImpl()
    private val gson = Gson()
    /**
     *  @param idCustomer
     *  @return Response
     *
     */
    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Allow to get a booking equipment by customer")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun bookingEquipmentByCustomerForAStore(@QueryParam("idCustomer") idCustomer: Long): Response? {
        return try {
            val booking = customerService?.getBookingEquipmentsCustomer(idCustomer)
            Response.ok(gson.toJson(booking), MediaType.APPLICATION_JSON).build()
        } catch (e: StoreFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        } catch (e: Exception) {
            Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        }
    }
}