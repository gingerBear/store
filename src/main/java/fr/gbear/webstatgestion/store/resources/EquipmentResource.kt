package fr.gbear.webstatgestion.store.resources

import com.codahale.metrics.annotation.Timed
import com.google.gson.Gson
import fr.gbear.webstatgestion.store.db.dto.EquipmentEditDto
import fr.gbear.webstatgestion.store.db.enumeration.StateEquipment
import fr.gbear.webstatgestion.store.service.EquipmentService
import fr.gbear.webstatgestion.store.service.impl.EquipmentServiceImpl
import fr.gbear.webstatgestion.store.util.annotation.ResourcesRest
import fr.gbear.webstatgestion.store.util.exception.equipment.EquipmentFindException
import fr.gbear.webstatgestion.store.util.exception.store.StoreFindException
import io.dropwizard.hibernate.UnitOfWork
import io.swagger.annotations.*
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@ResourcesRest
@Path("/stores/{idStore}/equipments")
@Api(value = "//stores/{idStore}/equipment", description = "Allows to manage a equipment")
@SwaggerDefinition
class EquipmentResource {
    private var equipmentService: EquipmentService? = EquipmentServiceImpl()
    private val gson = Gson()


    /**
     *  @param equipment
     *  @param idStore
     *  @return Response
     *
     *  Allows to create a equipment. If the model don't created then it will be create
     *  A state will be attached  to equipment
     */
    @POST
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Create an equipment")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun createEquipment(@PathParam("idStore") idStore: Long, equipment: EquipmentEditDto): Response? {
        return try {
            val newEquipment = equipmentService?.createEquipment(idStore, equipment)
            Response.ok(gson.toJson(newEquipment), MediaType.APPLICATION_JSON).build()
        } catch (e: StoreFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        }
    }


    @PUT
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "update an equipment\n" +
            "Query parameter : \n\t - state -> state's equipment")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    @Path("/{id}")
    fun updateEquipment(@PathParam("id") idEquipment: Long, @PathParam("idStore") idStore: Long, equipment: EquipmentEditDto, @QueryParam("state") stateEquipment: StateEquipment?): Response? {
        return try {
            equipment.idEquipment = idEquipment
            val newEquipment = equipmentService?.updateEquipment(idStore, equipment, stateEquipment)
            Response.ok(gson.toJson(newEquipment), MediaType.APPLICATION_JSON).build()
        } catch (e: StoreFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        } catch (e: EquipmentFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        }
    }

    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get all equpments of a store")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    fun getEquipments(@PathParam("idStore") idStore: Long): Response? {
        return try {
            val newEquipment = equipmentService?.getEquipments(idStore)
            Response.ok(gson.toJson(newEquipment), MediaType.APPLICATION_JSON).build()
        } catch (e: StoreFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        }
    }

    @DELETE
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "delete equipment of a store")
    @ApiResponses(value = arrayOf(
            ApiResponse(code = 400, message = "Error in persistence"),
            ApiResponse(code = 200, message = "Success"))
    )
    @UnitOfWork
    @Path("/{id}")
    fun deleteEquipment(@PathParam("idStore") idStore: Long, @PathParam("id") idEquipment: Long): Response? {
        return try {
            val isDeleted = equipmentService?.deleteEquipment(idStore, idEquipment)
            Response.ok(gson.toJson(isDeleted), MediaType.APPLICATION_JSON).build()
        } catch (e: StoreFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        } catch (e: EquipmentFindException) {
            Response.status(Response.Status.NOT_FOUND).entity(e.message).type(MediaType.TEXT_PLAIN_TYPE).build()
        }
    }
}