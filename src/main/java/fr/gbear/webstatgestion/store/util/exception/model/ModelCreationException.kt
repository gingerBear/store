package fr.gbear.webstatgestion.store.util.exception.model

class ModelCreationException(message: String?) : Exception(message)