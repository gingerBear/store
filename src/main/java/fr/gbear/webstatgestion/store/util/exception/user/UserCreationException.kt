package fr.gbear.webstatgestion.store.util.exception.user

class UserCreationException(message: String?) : Exception(message)