package fr.gbear.webstatgestion.store.util.exception.store

class StoreFindException(message: String?) : Exception(message)