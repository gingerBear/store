package fr.gbear.webstatgestion.store.util.exception.equipment

class EquipmentStatusCreationException : Exception("An error occurred while creating status equipment\n")