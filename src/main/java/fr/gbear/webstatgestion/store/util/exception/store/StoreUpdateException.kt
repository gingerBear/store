package fr.gbear.webstatgestion.store.util.exception.store

class StoreUpdateException(message: String?) : Exception(message)