package fr.gbear.webstatgestion.store.util.exception.model

class ModelUpdateException(override val message: String?) : Exception(message)