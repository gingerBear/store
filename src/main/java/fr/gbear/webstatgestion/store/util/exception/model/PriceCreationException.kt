package fr.gbear.webstatgestion.store.util.exception.model

class PriceCreationException(message: String?) : Exception(message)