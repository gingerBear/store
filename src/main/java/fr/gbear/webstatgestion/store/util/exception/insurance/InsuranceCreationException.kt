package fr.gbear.webstatgestion.store.util.exception.insurance

class InsuranceCreationException(m: String = "") : Exception("An error occurred while creating insurance : $m \n")