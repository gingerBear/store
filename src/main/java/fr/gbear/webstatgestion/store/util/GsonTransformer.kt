package fr.gbear.webstatgestion.store.util

class GsonTransformer private constructor() {
    companion object {
        private lateinit var instance: GsonTransformer
        @JvmStatic
        fun getInstance(): GsonTransformer {
            if (instance == null) {
                instance = GsonTransformer()
            }
            return instance
        }
    }
}