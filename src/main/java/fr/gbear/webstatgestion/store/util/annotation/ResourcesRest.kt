package fr.gbear.webstatgestion.store.util.annotation

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class ResourcesRest