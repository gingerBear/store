package fr.gbear.webstatgestion.store.util.exception.model

class ModelFindException(override val message: String?) : Exception(message)