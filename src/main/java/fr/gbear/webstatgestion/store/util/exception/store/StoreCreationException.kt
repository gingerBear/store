package fr.gbear.webstatgestion.store.util.exception.store

class StoreCreationException(message: String?) : Exception(message)