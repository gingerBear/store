package fr.gbear.webstatgestion.store.util.exception.equipment

class EquipmentBookingException : Exception("An error occurred while creation or update booking equipment\n")