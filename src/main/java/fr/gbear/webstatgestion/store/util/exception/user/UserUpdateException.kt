package fr.gbear.webstatgestion.store.util.exception.user

class UserUpdateException(override val message: String?) : Exception(message)