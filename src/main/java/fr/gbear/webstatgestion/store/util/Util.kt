package fr.gbear.webstatgestion.store.util

import com.google.common.collect.ImmutableList
import fr.gbear.webstatgestion.store.util.annotation.EntityBdd
import fr.gbear.webstatgestion.store.util.annotation.EntityDao
import fr.gbear.webstatgestion.store.util.annotation.ResourcesRest
import org.reflections.Reflections
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class Util {
    companion object {
        fun getClassEntities(packageName: String): ImmutableList<Class<*>>? {
            val reflections = Reflections(packageName)
            return ImmutableList.copyOf(reflections.getTypesAnnotatedWith(EntityBdd::class.java))
        }

        fun getClassResources(packageName: String): ImmutableList<Class<*>>? {
            val reflections = Reflections(packageName)
            return ImmutableList.copyOf(reflections.getTypesAnnotatedWith(ResourcesRest::class.java))
        }

        fun getClassDao(packageName: String): ImmutableList<Class<*>>? {
            val reflections = Reflections(packageName)
            return ImmutableList.copyOf(reflections.getTypesAnnotatedWith(EntityDao::class.java))
        }

        fun isValidString(string: String): Boolean {
            return string.trim() != ""
        }

        fun formatterDate(date: Date): Date {
            val formatter: DateFormat = SimpleDateFormat("dd/MM/yyyy")
            return formatter.parse(formatter.format(date))
        }
    }
}