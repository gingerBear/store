package fr.gbear.webstatgestion.store.util.exception.insurance

class InsuranceUpdateException(m: String = "") : Exception("An error occurred while updating a insurance : $m")