package fr.gbear.webstatgestion.store.util.exception.equipment

class EquipmentUpdateException : Exception("An error occurred while updating equipment\n")