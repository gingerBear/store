package fr.gbear.webstatgestion.store.util.exception.user

class UserFindException(override val message: String?) : Exception(message)