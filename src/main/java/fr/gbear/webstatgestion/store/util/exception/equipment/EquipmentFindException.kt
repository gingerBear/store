package fr.gbear.webstatgestion.store.util.exception.equipment

class EquipmentFindException : Exception("An error occurred while founding a equipment")