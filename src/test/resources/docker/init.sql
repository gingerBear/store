CREATE DATABASE webstat;
GRANT ALL PRIVILEGES ON DATABASE webstat TO postgres;
ALTER USER postgres PASSWORD 'webstat';

 \connect webstat;
CREATE SCHEMA store;
CREATE SCHEMA esf;
CREATE SCHEMA lift;
CREATE SCHEMA customer;
CREATE SCHEMA rental;
CREATE SCHEMA auth;

GRANT ALL PRIVILEGES ON SCHEMA store TO postgres ;
GRANT ALL PRIVILEGES ON SCHEMA esf TO postgres ;
GRANT ALL PRIVILEGES ON SCHEMA lift TO postgres ;
GRANT ALL PRIVILEGES ON SCHEMA customer TO postgres ;
GRANT ALL PRIVILEGES ON SCHEMA rental TO postgres ;
GRANT ALL PRIVILEGES ON SCHEMA auth TO  postgres ;
