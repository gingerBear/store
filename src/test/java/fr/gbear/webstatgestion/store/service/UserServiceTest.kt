package fr.gbear.webstatgestion.store.service

import fr.gbear.webstatgestion.store.db.entity.User
import fr.gbear.webstatgestion.store.service.impl.UserServiceImpl
import fr.gbear.webstatgestion.store.util.exception.user.UserCreationException
import fr.gbear.webstatgestion.store.util.exception.user.UserFindException
import fr.gbear.webstatgestion.store.util.exception.user.UserUpdateException
import io.dropwizard.testing.junit.DAOTestRule
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


class UserServiceTest {

    @get:Rule
    var database = DAOTestRule.newBuilder().addEntityClass(User::class.java).build()!!

    private lateinit var userServiceImpl: UserServiceImpl

    @Before
    fun setUp() {
        userServiceImpl = UserServiceImpl(database.sessionFactory)
    }

    @Test
    fun constructorTest() {
        UserServiceImpl()
    }

    @Test
    fun createUserTest_OK() {
        val user = User()
        user.firstName = "unitTest"
        user.lastName = "unitTest"
        user.arrivalDate = Date()
        user.isAdmin = false
        user.idStore = 1
        user.email = "test@test.fr"
        val newUser = userServiceImpl.createUser(user)
        Assert.assertTrue(newUser.idUser != null)
    }

    @Test
    fun updateUserTest_OK() {
        val user = User()
        user.firstName = "unitTest2"
        user.lastName = "unitTest2"
        user.arrivalDate = Date()
        user.isAdmin = false
        user.idStore = 1
        user.email = "test@test.fr"
        val newUser = userServiceImpl.createUser(user)
        Assert.assertTrue(newUser.idUser != null)
        newUser.firstName = "unitTestUpdate"
        newUser.lastName = "unitTestUpdate"
        newUser.isAdmin = true
        val userUpdated = userServiceImpl.updateUser(newUser)
        Assert.assertTrue(newUser.isAdmin)
        Assert.assertTrue(newUser.lastName == userUpdated.lastName)
        Assert.assertTrue(newUser.lastName == userUpdated.lastName)
    }

    @Test(expected = UserUpdateException::class)
    fun updateUserTest_KO_UserUpdateException() {
        val user = User()
        user.firstName = "unitTest3"
        user.lastName = "unitTest3"
        user.arrivalDate = Date()
        user.isAdmin = false
        user.idStore = 1
        user.email = "test@test.fr"
        val newUser = userServiceImpl.createUser(user)
        Assert.assertTrue(newUser.idUser != null)
        newUser.firstName = ""
        newUser.lastName = "unitTestUpdate"
        newUser.isAdmin = true
        val userUpdated = userServiceImpl.updateUser(newUser)
        Assert.assertTrue(newUser.isAdmin)
        Assert.assertTrue(newUser.lastName == userUpdated.lastName)
        Assert.assertTrue(newUser.lastName == userUpdated.lastName)
    }

    @Test(expected = UserFindException::class)
    fun updateUserTest_KO_UserFindException() {
        val user = User()
        user.firstName = "unitTest3"
        user.lastName = "unitTest3"
        user.arrivalDate = Date()
        user.isAdmin = false
        user.idStore = 1
        user.email = "test@test.fr"
        val newUser = userServiceImpl.createUser(user)
        Assert.assertTrue(newUser.idUser != null)
        newUser.firstName = "unitTestUpdate"
        newUser.lastName = "unitTestUpdate"
        newUser.isAdmin = true
        newUser.idUser = 9000000000
        val userUpdated = userServiceImpl.updateUser(newUser)
        Assert.assertTrue(newUser.isAdmin)
        Assert.assertTrue(newUser.lastName == userUpdated.lastName)
        Assert.assertTrue(newUser.lastName == userUpdated.lastName)
    }

    @Test(expected = UserCreationException::class)
    fun createUserTest_KO_UserCreationException() {
        val user = User()
        user.firstName = "unitTest"
        user.arrivalDate = Date()
        user.isAdmin = false
        user.idStore = 1
        user.email = "test@test.fr"
        userServiceImpl.createUser(user)
    }

    @Test
    fun getUsers() {
        var user = User()
        user.firstName = "unitTest"
        user.lastName = "unitTest"
        user.arrivalDate = Date()
        user.isAdmin = false
        user.idStore = 1
        user.email = "test@test.fr"
        val newUser = userServiceImpl.createUser(user)

        Assert.assertTrue(newUser.idUser != null)
        user = User()
        user.firstName = "unitTest"
        user.lastName = "unitTest"
        user.arrivalDate = Date()
        user.isAdmin = false
        user.idStore = 1
        user.email = "test2@test.fr"
        val newUser2 = userServiceImpl.createUser(user)

        Assert.assertTrue(newUser2.idUser != null)

        val users = userServiceImpl.getUsers()
        assertEquals(2, users.size)
    }

    @Test(expected = UserFindException::class)
    fun getUserByEmailTest_KO_UserFindException() {
        userServiceImpl.getUserByEmail("t1@t1.fr")
    }

    @Test
    fun getUserByEmailTest_OK() {
        val user = User()
        user.firstName = "unitTest"
        user.lastName = "unitTest"
        user.arrivalDate = Date()
        user.isAdmin = false
        user.idStore = 1
        user.email = "test2@test.fr"
        val newUser2 = userServiceImpl.createUser(user)

        val getUser = userServiceImpl.getUserByEmail("test2@test.fr")
        assertNotNull(getUser)
        assert(getUser.idUser == newUser2.idUser)
    }

}