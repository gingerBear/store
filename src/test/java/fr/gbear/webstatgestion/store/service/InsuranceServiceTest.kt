package fr.gbear.webstatgestion.store.service

import fr.gbear.webstatgestion.store.db.entity.Insurance
import fr.gbear.webstatgestion.store.db.entity.Store
import fr.gbear.webstatgestion.store.service.impl.InsuranceServiceImpl
import fr.gbear.webstatgestion.store.service.impl.StoreServiceImpl
import fr.gbear.webstatgestion.store.util.exception.insurance.InsuranceCreationException
import fr.gbear.webstatgestion.store.util.exception.insurance.InsuranceFindException
import fr.gbear.webstatgestion.store.util.exception.insurance.InsuranceUpdateException
import fr.gbear.webstatgestion.store.util.exception.store.StoreFindException
import io.dropwizard.testing.junit.DAOTestRule
import org.junit.Assert
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertFalse

class InsuranceServiceTest {
    @get:Rule
    var database = DAOTestRule.newBuilder().addEntityClass(Store::class.java).addEntityClass(Insurance::class.java).build()!!

    private lateinit var insuranceService: InsuranceService
    private lateinit var storeServiceImpl: StoreService
    private lateinit var store: Store

    private fun createStore(): Store {
        val storeTmp = Store()
        storeTmp.address = "adress"
        storeTmp.city = "city"
        storeTmp.cp = "cp"
        storeTmp.name = "store"
        store = storeServiceImpl.createStore(storeTmp)
        return store
    }

    private fun createInsurance(name: String = "insurance", isActive: Boolean = true, price: Double? = null, insuranceRate: Double? = null, storeId: Long = store.idStore!!): Insurance {
        val insurance = Insurance()
        insurance.isActive = isActive
        insurance.price = price
        insurance.name = name
        insurance.insuranceRate = insuranceRate
        insurance.idStore = storeId
        return insurance
    }

    @Before
    fun setUp() {
        storeServiceImpl = StoreServiceImpl(database.sessionFactory)
        insuranceService = InsuranceServiceImpl(database.sessionFactory)
        store = createStore()
    }

    @Test(expected = StoreFindException::class)
    fun createInsuranceTest_KO_StoreFindException() {
        val insurance = createInsurance(storeId = store.idStore!!)
        insuranceService.createAnInsurance(0, insurance)
    }

    @Test
    fun createInsurance_OK() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val result = insuranceService.createAnInsurance(store.idStore!!, insurance)
        assertTrue(result.idInsurance != null)
    }

    @Test(expected = InsuranceCreationException::class)
    fun createInsurance_KO_InsuranceCreationException() {
        val insurance = createInsurance(storeId = store.idStore!!)
        val result = insuranceService.createAnInsurance(store.idStore!!, insurance)
        assertTrue(result.idInsurance != null)
    }

    @Test(expected = StoreFindException::class)
    fun updateInsuranceTest_KO_StoreFindException() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val result = insuranceService.createAnInsurance(store.idStore!!, insurance)
        assertTrue(result.idInsurance != null)
        result.name = "toto"
        insuranceService.updateAnInsurance(0, result)
    }

    @Test
    fun updateInsurance_OK() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val newInsurance = insuranceService.createAnInsurance(store.idStore!!, insurance)
        assertTrue(newInsurance.idInsurance != null)
        newInsurance.name = "toto"
        newInsurance.price = null
        newInsurance.insuranceRate = 20.0
        val result = insuranceService.updateAnInsurance(store.idStore!!, newInsurance)
        assertTrue(result.idInsurance != null)
        assertTrue(result.name == "toto")
        assertTrue(result.price == null)
        assertTrue(result.insuranceRate == newInsurance.insuranceRate)
    }

    @Test(expected = InsuranceUpdateException::class)
    fun updateInsurance_KO_InsuranceUpdateException() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val newInsurance = insuranceService.createAnInsurance(store.idStore!!, insurance)
        assertTrue(newInsurance.idInsurance != null)
        newInsurance.name = "toto"
        newInsurance.insuranceRate = 20.0
        insuranceService.updateAnInsurance(store.idStore!!, newInsurance)
    }

    @Test
    fun getInsurance_OK() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val result = insuranceService.createAnInsurance(store.idStore!!, insurance)
        Assert.assertTrue(result.idInsurance != null)
        val getInsurance = insuranceService.getInsurance(store.idStore!!, result.idInsurance!!)
        assertTrue(getInsurance == result)
    }

    @Test(expected = StoreFindException::class)
    fun getInsurance_KO_StoreFindException() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val result = insuranceService.createAnInsurance(store.idStore!!, insurance)
        Assert.assertTrue(result.idInsurance != null)
        insuranceService.getInsurance(0, result.idInsurance!!)
    }

    @Test(expected = InsuranceFindException::class)
    fun getInsurance_KO_InsuranceFindException() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val result = insuranceService.createAnInsurance(store.idStore!!, insurance)
        Assert.assertTrue(result.idInsurance != null)
        insuranceService.getInsurance(store.idStore!!, 0)
    }

    @Test
    fun getInsurances_OK() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val result = insuranceService.createAnInsurance(store.idStore!!, insurance)
        Assert.assertTrue(result.idInsurance != null)
        val getInsurances = insuranceService.getInsurances(store.idStore!!)
        assertTrue(getInsurances.size == 1)
    }

    @Test(expected = StoreFindException::class)
    fun getInsurances_KO_StoreFIndException() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val result = insuranceService.createAnInsurance(store.idStore!!, insurance)
        Assert.assertTrue(result.idInsurance != null)
        insuranceService.getInsurances(0)
    }

    @Test
    fun checkInsurance_OK() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val res = insuranceService.checkInsurance(store.idStore!!, insurance)
        assertTrue(res)
    }

    @Test
    fun checkInsurance_FALSE_Store() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val res = insuranceService.checkInsurance(0, insurance)
        assertFalse(res)
    }

    @Test
    fun checkInsurance_FALSE_priceAndRateNotnull() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0, insuranceRate = 20.0)
        val res = insuranceService.checkInsurance(0, insurance)
        assertFalse(res)
    }

    @Test
    fun checkInsurance_FALSE_priceAndRateNull() {
        val insurance = createInsurance(storeId = store.idStore!!)
        val res = insuranceService.checkInsurance(0, insurance)
        assertFalse(res)
    }

    @Test
    fun deleteInsurance_ok() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val result = insuranceService.createAnInsurance(store.idStore!!, insurance)
        Assert.assertTrue(result.idInsurance != null)
        val insuranceDeleted = insuranceService.deleteInsurance(store.idStore!!, result.idInsurance!!)
        assertFalse { insuranceDeleted.isActive!! }
    }

    @Test(expected = StoreFindException::class)
    fun deleteInsurance_ko_storeFindException() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val result = insuranceService.createAnInsurance(store.idStore!!, insurance)
        Assert.assertTrue(result.idInsurance != null)
        insuranceService.deleteInsurance(0, result.idInsurance!!)
    }

    @Test(expected = InsuranceFindException::class)
    fun deleteInsurance_ko_insuranceFindException() {
        val insurance = createInsurance(storeId = store.idStore!!, price = 5.0)
        val result = insuranceService.createAnInsurance(store.idStore!!, insurance)
        Assert.assertTrue(result.idInsurance != null)
        insuranceService.deleteInsurance(store.idStore!!, 0)
    }
}