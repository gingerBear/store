package fr.gbear.webstatgestion.store.service

import fr.gbear.webstatgestion.store.db.dto.EquipmentBookingEditDto
import fr.gbear.webstatgestion.store.db.dto.EquipmentEditDto
import fr.gbear.webstatgestion.store.db.dto.ModelEditDto
import fr.gbear.webstatgestion.store.db.entity.*
import fr.gbear.webstatgestion.store.db.enumeration.StateEquipment
import fr.gbear.webstatgestion.store.service.impl.EquipmentServiceImpl
import fr.gbear.webstatgestion.store.service.impl.StoreServiceImpl
import fr.gbear.webstatgestion.store.util.exception.equipment.EquipmentBookingException
import fr.gbear.webstatgestion.store.util.exception.equipment.EquipmentCreationException
import fr.gbear.webstatgestion.store.util.exception.equipment.EquipmentFindException
import fr.gbear.webstatgestion.store.util.exception.equipment.EquipmentStatusCreationException
import fr.gbear.webstatgestion.store.util.exception.store.StoreFindException
import io.dropwizard.testing.junit.DAOTestRule
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.sql.Timestamp
import java.util.*

class EquipmentServiceTest {
    @get:Rule
    var database = DAOTestRule.newBuilder().addEntityClass(Store::class.java).addEntityClass(Equipment::class.java).addEntityClass(Booking::class.java)
            .addEntityClass(BookingEquipment::class.java).addEntityClass(Insurance::class.java).addEntityClass(Model::class.java).addEntityClass(StatusEquipment::class.java).addEntityClass(Price::class.java).build()!!

    private lateinit var equipmentService: EquipmentService
    private lateinit var storeServiceImpl: StoreService
    private lateinit var store: Store

    @Before
    fun setUp() {
        storeServiceImpl = StoreServiceImpl(database.sessionFactory)
        equipmentService = EquipmentServiceImpl(database.sessionFactory)
        store = createStore()
    }

    private fun createStore(): Store {
        val storeTmp = Store()
        storeTmp.address = "adress"
        storeTmp.city = "city"
        storeTmp.cp = "cp"
        storeTmp.name = "store"
        store = storeServiceImpl.createStore(storeTmp)
        return store
    }

    private fun createEquipmentDto(name: String = "model",
                                   modelS: String = "ski",
                                   brand: String = "brand",
                                   barcode: Long = 123,
                                   description: String = "description",
                                   priceNb: Double = 30.0,
                                   size: Int = 20
    ): EquipmentEditDto {
        val equipmentEditDto = EquipmentEditDto()
        val model = ModelEditDto()
        model.reference = name
        model.model = modelS
        model.brand = brand
        model.barcode = barcode
        model.description = description
        equipmentEditDto.modelEditDto = model
        equipmentEditDto.shoeSize = size
        val price = Price()
        price.price = priceNb
        price.date = Timestamp(Date().time)
        equipmentEditDto.price = price
        return equipmentEditDto
    }

    fun createEquipmentBookingDto(equipment: Equipment, beginDate: Date = Date(), endDate: Date = Date()): EquipmentBookingEditDto {
        val equipmentBooking = EquipmentBookingEditDto()
        equipmentBooking.idCustomer = 1
        equipmentBooking.idEquipment = equipment.idEquipment
        equipmentBooking.beginDate = beginDate
        equipmentBooking.endDate = endDate
        return equipmentBooking
    }

    @Test
    fun createEquipmentTest_OK() {
        val equipmentEditDto = createEquipmentDto()
        val newEquipment = equipmentService.createEquipment(store.idStore!!, equipmentEditDto)
        Assert.assertTrue(newEquipment.idEquipment != null)
    }

    @Test(expected = StoreFindException::class)
    fun createEquipmentTest_KO_StoreFindException() {
        val equipmentEditDto = createEquipmentDto()
        equipmentService.createEquipment(0, equipmentEditDto)
    }

    @Test(expected = EquipmentCreationException::class)
    fun createEquipmentTest_KO_EquipmentCreationException() {
        val equipmentEditDto = createEquipmentDto()
        equipmentEditDto.price = Price()
        equipmentService.createEquipment(store.idStore!!, equipmentEditDto)
    }

    @Test
    fun createEquipmentTest_OK_NewModel() {
        val equipmentEditDto = createEquipmentDto()
        Assert.assertTrue(equipmentEditDto.modelEditDto!!.idModel == null)
        val newEquipment = equipmentService.createEquipment(store.idStore!!, equipmentEditDto)
        Assert.assertTrue(newEquipment.idEquipment != null)
        Assert.assertTrue(newEquipment.model!!.idModel != null)
    }

    @Test
    fun updateEquipmentTest_OK() {
        val equipmentEditDto = createEquipmentDto()
        Assert.assertTrue(equipmentEditDto.modelEditDto!!.idModel == null)
        val newEquipment = equipmentService.createEquipment(store.idStore!!, equipmentEditDto)
        Assert.assertTrue(newEquipment.idEquipment != null)
        val updateEquipment = createEquipmentDto("modelUpdate", "modelUpdate", "modelUpdate", 12, "modelUpdate", 33.2, 32)
        updateEquipment.idEquipment = newEquipment.idEquipment
        updateEquipment.modelEditDto!!.idModel = newEquipment.model!!.idModel
        val newUpdate = equipmentService.updateEquipment(store.idStore!!, updateEquipment)
        Assert.assertTrue(newUpdate.idEquipment != null)
        Assert.assertTrue(newUpdate.idEquipment == newEquipment.idEquipment)
        Assert.assertTrue(newUpdate.model!!.idModel == newEquipment.model!!.idModel)
    }

    @Test(expected = StoreFindException::class)
    fun updateEquipmentTest_KO_StoreFindException() {
        val equipmentEditDto = createEquipmentDto()
        equipmentService.updateEquipment(0, equipmentEditDto)
    }

    @Test(expected = EquipmentFindException::class)
    fun updateEquipmentTest_KO_EquipmentFindException() {
        val equipmentEditDto = createEquipmentDto()
        equipmentService.updateEquipment(store.idStore!!, equipmentEditDto)
    }

    @Test
    fun updateEquipmentTest_OK_NewModel() {
        val equipmentEditDto = createEquipmentDto()
        Assert.assertTrue(equipmentEditDto.modelEditDto!!.idModel == null)
        val newEquipment = equipmentService.createEquipment(store.idStore!!, equipmentEditDto)
        Assert.assertTrue(newEquipment.idEquipment != null)
        val idModel = newEquipment.model!!.idModel
        val updateEquipment = createEquipmentDto("modelUpdate", "modelUpdate", "modelUpdate", 12, "modelUpdate", 33.2, 32)
        updateEquipment.idEquipment = newEquipment.idEquipment
        val newUpdate = equipmentService.updateEquipment(store.idStore!!, updateEquipment)
        Assert.assertTrue(newUpdate.idEquipment != null)
        Assert.assertTrue(newUpdate.idEquipment == newEquipment.idEquipment)
        Assert.assertTrue(newUpdate.model!!.idModel!! > idModel!!)
    }

    @Test
    fun getEquipments_OK() {
        val equipmentEditDto = createEquipmentDto()
        Assert.assertTrue(equipmentEditDto.modelEditDto!!.idModel == null)
        val newEquipment = equipmentService.createEquipment(store.idStore!!, equipmentEditDto)
        Assert.assertTrue(newEquipment.idEquipment != null)
        val equipments = equipmentService.getEquipments(store.idStore!!)
        Assert.assertTrue(equipments.size == 1)
        Assert.assertTrue(equipments[0].equipments.size == 1)
    }

    @Test(expected = StoreFindException::class)
    fun getEquipments_KO_StoreFindException() {
        val equipmentEditDto = createEquipmentDto()
        Assert.assertTrue(equipmentEditDto.modelEditDto!!.idModel == null)
        val newEquipment = equipmentService.createEquipment(store.idStore!!, equipmentEditDto)
        Assert.assertTrue(newEquipment.idEquipment != null)
        equipmentService.getEquipments(0)
    }

    @Test
    fun deleteEquipmentTest_OK() {
        val equipmentEditDto = createEquipmentDto()
        Assert.assertTrue(equipmentEditDto.modelEditDto!!.idModel == null)
        val newEquipment = equipmentService.createEquipment(store.idStore!!, equipmentEditDto)
        Assert.assertTrue(newEquipment.idEquipment != null)
        val equipments = equipmentService.getEquipments(store.idStore!!)
        Assert.assertTrue(equipments.size == 1)
        val isDelete = equipmentService.deleteEquipment(store.idStore!!, newEquipment.idEquipment!!)
        Assert.assertTrue(isDelete)
    }

    @Test(expected = StoreFindException::class)
    fun deleteEquipmentTest_KO_StoreFindException() {
        equipmentService.deleteEquipment(0, 0)
    }

    @Test(expected = EquipmentFindException::class)
    fun deleteEquipmentTest_KO_EquipmentFindException() {
        equipmentService.deleteEquipment(store.idStore!!, 0)
    }

    @Test(expected = EquipmentStatusCreationException::class)
    fun createStatusEquipment_KO_EquipmentStatusCreationException() {
        val equipmentEditDto = createEquipmentDto()
        val newEquipment = equipmentService.createEquipment(store.idStore!!, equipmentEditDto)
        equipmentService.createStatusEquipment(newEquipment, StateEquipment.NEW)
    }

    @Test(expected = StoreFindException::class)
    fun bookingEquipments_KO_StoreFindException() {
        val equipmentEditDto = createEquipmentDto()
        val equipment = equipmentService.createEquipment(store.idStore!!, equipmentEditDto)
        val equipmentEditDtos: Set<EquipmentBookingEditDto> = hashSetOf(createEquipmentBookingDto(equipment, Date(), Date()))
        equipmentService.bookingEquipments(0, equipmentEditDtos.toList())
    }

    @Test(expected = EquipmentBookingException::class)
    fun bookingEquipments_KO_EquipmentBookingException() {
        val equipmentEditDto = createEquipmentDto()
        val equipment = equipmentService.createEquipment(store.idStore!!, equipmentEditDto)
        val bookings: Set<EquipmentBookingEditDto> = hashSetOf(createEquipmentBookingDto(equipment, Date(), Date()))
        val equipments = equipmentService.bookingEquipments(store.idStore!!, bookings.toList())
        Assert.assertTrue(equipments.size == 1)
        Assert.assertTrue(equipments[0].idEquipment == equipment.idEquipment)
        Assert.assertTrue(equipments[0].bookings!!.size == 1)
        equipmentService.bookingEquipments(store.idStore!!, bookings.toList())
    }

    @Test
    fun bookingEquipments_OK() {
        val equipmentEditDto = createEquipmentDto()
        val equipment = equipmentService.createEquipment(store.idStore!!, equipmentEditDto)
        val bookings: Set<EquipmentBookingEditDto> = hashSetOf(createEquipmentBookingDto(equipment, Date(), Date()))
        val equipments = equipmentService.bookingEquipments(store.idStore!!, bookings.toList())
        Assert.assertTrue(equipments.size == 1)
        Assert.assertTrue(equipments[0].idEquipment == equipment.idEquipment)
        Assert.assertTrue(equipments[0].bookings!!.size == 1)
    }


}