package fr.gbear.webstatgestion.store.service

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(UserServiceTest::class, StoreServiceTest::class, ModelServiceTest::class, EquipmentServiceTest::class, InsuranceServiceTest::class)
class SuiteTest