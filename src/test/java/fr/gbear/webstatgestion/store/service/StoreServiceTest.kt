package fr.gbear.webstatgestion.store.service

import fr.gbear.webstatgestion.store.db.entity.Store
import fr.gbear.webstatgestion.store.service.impl.StoreServiceImpl
import fr.gbear.webstatgestion.store.util.exception.store.StoreCreationException
import fr.gbear.webstatgestion.store.util.exception.store.StoreFindException
import fr.gbear.webstatgestion.store.util.exception.store.StoreUpdateException
import io.dropwizard.testing.junit.DAOTestRule
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertEquals

class StoreServiceTest {
    @get:Rule
    var database = DAOTestRule.newBuilder().addEntityClass(Store::class.java).build()!!

    private lateinit var storeServiceImpl: StoreService

    @Before
    fun setUp() {
        storeServiceImpl = StoreServiceImpl(database.sessionFactory)
    }

    @Test
    fun createStoreTest_OK() {
        val store = Store()
        store.address = "adress"
        store.city = "city"
        store.cp = "cp"
        store.name = "store"
        val newStore = storeServiceImpl.createStore(store)
        Assert.assertTrue(newStore.idStore != null)
    }

    @Test
    fun updateStoreTest_OK() {
        val store = Store()
        store.address = "adress"
        store.city = "city"
        store.cp = "cp"
        store.name = "store"
        val newStore = storeServiceImpl.createStore(store)
        Assert.assertTrue(newStore.idStore != null)
        newStore.address = "adressUpdate"
        newStore.city = "cityUpdate"
        newStore.cp = "cpUpdate"
        newStore.name = "update"
        val storeUpdated = storeServiceImpl.updateStore(newStore)
        Assert.assertTrue(newStore.name == storeUpdated.name)
        Assert.assertTrue(newStore.city == storeUpdated.city)
        Assert.assertTrue(newStore.cp == storeUpdated.cp)
    }

    @Test(expected = StoreUpdateException::class)
    fun updateStoreTest_KO_StoreUpdateException() {
        val store = Store()
        store.address = "adress"
        store.city = "city"
        store.cp = "cp"
        store.name = "store"
        val newStore = storeServiceImpl.createStore(store)
        Assert.assertTrue(newStore.idStore != null)
        newStore.address = "adressUpdate"
        newStore.city = "cityUpdate"
        newStore.cp = "cpUpdate"
        newStore.name = ""
        storeServiceImpl.updateStore(newStore)
    }

    @Test(expected = StoreFindException::class)
    fun updateStoreTest_KO_StoreFindException() {
        val store = Store()
        store.address = "adress"
        store.city = "city"
        store.cp = "cp"
        store.name = "store"
        val newStore = storeServiceImpl.createStore(store)
        Assert.assertTrue(newStore.idStore != null)
        Assert.assertTrue(newStore.idStore != null)
        newStore.address = "adressUpdate"
        newStore.city = "cityUpdate"
        newStore.cp = "cpUpdate"
        newStore.name = "update"
        newStore.idStore = 9999999999999999
        storeServiceImpl.updateStore(newStore)
    }

    @Test(expected = StoreCreationException::class)
    fun createStoreTest_KO_StoreCreationException() {
        val store = Store()
        store.address = "adress"
        store.city = "city"
        store.cp = "cp"
        storeServiceImpl.createStore(store)
    }

    @Test
    fun getStoresTest_OK() {
        var store = Store()
        store.address = "adress"
        store.city = "city"
        store.cp = "cp"
        store.name = "store"
        val newStore = storeServiceImpl.createStore(store)

        Assert.assertTrue(newStore.idStore != null)
        store = Store()
        store.address = "adress2"
        store.city = "city2"
        store.cp = "cp2"
        store.name = "store2"
        val newStore2 = storeServiceImpl.createStore(store)

        Assert.assertTrue(newStore2.idStore != null)

        val users = storeServiceImpl.getStores()
        assertEquals(2, users.size)

    }

    @Test
    fun getStore_OK() {
        val store = Store()
        store.address = "adress"
        store.city = "city"
        store.cp = "cp"
        store.name = "store"
        val newStore = storeServiceImpl.createStore(store)
        val id = newStore.idStore
        Assert.assertTrue(id != null)
        val getStore = storeServiceImpl.getStore(id!!)
        Assert.assertNotNull(getStore)
        Assert.assertTrue(id == getStore.idStore)
    }


    @Test(expected = StoreFindException::class)
    fun getStore_OK_KO_StoreFindException() {
        storeServiceImpl.getStore(9999999999999999)
    }
}