package fr.gbear.webstatgestion.store.service

import fr.gbear.webstatgestion.store.db.dto.ModelEditDto
import fr.gbear.webstatgestion.store.db.entity.Model
import fr.gbear.webstatgestion.store.db.entity.Store
import fr.gbear.webstatgestion.store.service.impl.ModelServiceImpl
import fr.gbear.webstatgestion.store.service.impl.StoreServiceImpl
import fr.gbear.webstatgestion.store.util.exception.model.ModelCreationException
import fr.gbear.webstatgestion.store.util.exception.model.ModelFindException
import fr.gbear.webstatgestion.store.util.exception.model.ModelUpdateException
import fr.gbear.webstatgestion.store.util.exception.store.StoreFindException
import io.dropwizard.testing.junit.DAOTestRule
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertTrue

class ModelServiceTest {

    @get:Rule
    var database = DAOTestRule.newBuilder().addEntityClass(Store::class.java).addEntityClass(Model::class.java).build()!!

    private lateinit var modelServiceImpl: ModelService
    private lateinit var storeServiceImpl: StoreService
    private lateinit var store: Store

    @Before
    fun setUp() {
        storeServiceImpl = StoreServiceImpl(database.sessionFactory)
        modelServiceImpl = ModelServiceImpl(database.sessionFactory)
        store = createStore()
    }

    private fun createStore(): Store {
        val storeTmp = Store()
        storeTmp.address = "adress"
        storeTmp.city = "city"
        storeTmp.cp = "cp"
        storeTmp.name = "store"
        store = storeServiceImpl.createStore(storeTmp)
        return store
    }

    private fun createModelDto(name: String = "model",
                               modelS: String = "ski",
                               brand: String = "brand",
                               barcode: Long = 123,
                               description: String = "description"
    ): ModelEditDto {
        val model = ModelEditDto()
        model.reference = name
        model.model = modelS
        model.brand = brand
        model.barcode = barcode
        model.description = description
        return model
    }

    @Test
    fun createService() {
        ModelServiceImpl()
        assertTrue { true }
    }

    @Test
    fun createModelTest_OK() {
        val model = createModelDto()
        val newModel = modelServiceImpl.createModel(store.idStore!!, model)
        Assert.assertTrue(newModel.idModel != null)
    }

    @Test(expected = StoreFindException::class)
    fun createModelTest_KO_StoreFindException() {
        val model = createModelDto()
        modelServiceImpl.createModel(0, model)
    }

    @Test(expected = ModelCreationException::class)
    fun createModelTest_KO_ModelCreationException_name() {
        val model = createModelDto()
        model.reference = ""
        modelServiceImpl.createModel(store.idStore!!, model)
    }

    @Test(expected = ModelCreationException::class)
    fun createModelTest_KO_ModelCreationException_model() {
        val model = createModelDto()
        model.model = ""
        modelServiceImpl.createModel(store.idStore!!, model)
    }

    @Test
    fun updateModelTest_OK() {
        val model = createModelDto()
        val newModel = modelServiceImpl.createModel(store.idStore!!, model)
        Assert.assertTrue(newModel.idModel != null)
        val modelUpdated = createModelDto("modelUpdate", "modelUpdate", "modelUpdate", 12, "modelUpdate")
        modelUpdated.idModel = newModel.idModel
        val newModelUpdated = modelServiceImpl.updateModel(store.idStore!!, modelUpdated)
        Assert.assertTrue(newModelUpdated.idModel != null)
        Assert.assertTrue(newModelUpdated.reference == "modelUpdate")
        Assert.assertTrue(newModelUpdated.model == "modelUpdate")
        Assert.assertTrue(newModelUpdated.brand == "modelUpdate")
        Assert.assertTrue(newModelUpdated.description == "modelUpdate")
        Assert.assertTrue(newModelUpdated.barcode!! == 12.toLong())
    }

    @Test(expected = StoreFindException::class)
    fun updateModelTest_KO_StoreFindException() {
        val model = createModelDto()
        modelServiceImpl.updateModel(0, model)
    }

    @Test(expected = ModelUpdateException::class)
    fun updateModelTest_KO_ModelCreationException_name() {
        val model = createModelDto()
        modelServiceImpl.updateModel(store.idStore!!, model)
    }

    @Test
    fun getModelsTest_OK() {
        val model = createModelDto()
        modelServiceImpl.createModel(store.idStore!!, model)
        val models = modelServiceImpl.getModels(store.idStore!!)
        assertTrue { models.size == 1 }
    }

    @Test(expected = StoreFindException::class)
    fun getModelsTest_KO() {
        modelServiceImpl.getModels(0)
    }

    @Test
    fun getModelById_OK() {
        val model = createModelDto()
        val newModel = modelServiceImpl.createModel(store.idStore!!, model)
        val getModel = modelServiceImpl.getModelById(store.idStore!!, newModel.idModel!!)
        assertTrue { getModel.idModel == newModel.idModel }
    }

    @Test(expected = StoreFindException::class)
    fun getModelById_KO_StoreFindException() {
        val model = createModelDto()
        val newModel = modelServiceImpl.createModel(store.idStore!!, model)
        modelServiceImpl.getModelById(0, newModel.idModel!!)
    }

    @Test(expected = ModelFindException::class)
    fun getModelById_KO_ModelFindException() {
        val model = createModelDto()
        modelServiceImpl.createModel(store.idStore!!, model)
        modelServiceImpl.getModelById(store.idStore!!, 0)
    }

}