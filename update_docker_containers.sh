#!/usr/bin/env bash
docker-compose pull;
docker-compose run  --entrypoint "java -jar /WS-store-back.jar db migrate config.yml" store-back;
docker-compose up -d;