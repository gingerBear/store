FROM openjdk:8-jdk-alpine
VOLUME /tmp
ENV JAR_FILE=target/store-*SNAPSHOT.jar
COPY ${JAR_FILE} WS-store-back.jar
COPY config.yml config.yml
#RUN java -jar /WS-store-back.jar db migrate config.yml
EXPOSE 9045 9046 8010 8011
ENTRYPOINT ["java", "-jar", "/WS-store-back.jar", "server", "config.yml"]