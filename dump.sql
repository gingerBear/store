--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1 (Debian 11.1-1.pgdg90+1)
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: store; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.store ("idStore", name, address, cp, city, latitude, longitude) FROM stdin;
1	Ski & Co	3 rue des moulineaux	45150	Belleville	\N	\N
2	Dusse's	5 rue blanche	45600	SkiCity	\N	\N
3	XTrem Snow	12 rue des flocons	45150	Belleville	\N	\N
4	Trotski	5 rue machin	45150	Belleville	\N	\N
5	Servietski	7 rue truc	45300	Maubeuge	\N	\N
6	Ca Glisse	45 bis rue quelque chose	45120	Doli	\N	\N
\.


--
-- Data for Name: booking; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.booking ("idBooking", "idCustomer", "idStore") FROM stdin;
\.


--
-- Data for Name: model; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.model ("idModel", reference, model, barcode, brand, "imagePath", description, "idStore") FROM stdin;
7	RF7	Skis alpin R320	7	POW	skis_alpin/r320	Integer at lorem dolor. Praesent a facilisis lectus, in eleifend dui. Fusce gravida, velit ac tincidunt mollis, leo dui euismod leo, a consequat eros urna nec leo. Nullam urna nisl, aliquet sit amet luctus sit amet, molestie id sem.	1
11	RF11	Skis alpin N5	11	HEAD	skis_alpin/n5	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam finibus hendrerit mattis. Pellentesque ut elit sollicitudin, iaculis quam vitae, maximus nulla.	1
13	RF13	Skis alpin N7	13	HEAD	skis_alpin/n7	Pellentesque porta pretium nibh. Nulla sed metus purus. Fusce ullamcorper ex vitae nisl vehicula, sed convallis odio aliquet. Morbi varius quis metus ut vestibulum.	1
14	RF14	Skis alpin 360B	14	BLIZZARD	skis_alpin/360b	Integer at lorem dolor. Praesent a facilisis lectus, in eleifend dui. Fusce gravida, velit ac tincidunt mollis, leo dui euismod leo, a consequat eros urna nec leo. Nullam urna nisl, aliquet sit amet luctus sit amet, molestie id sem.	1
8	RF8	Skis alpin XXX	8	POW	skis_alpin/xxx	Integer at lorem dolor. Praesent a facilisis lectus, in eleifend dui. Fusce gravida, velit ac tincidunt mollis, leo dui euismod leo, a consequat eros urna nec leo. Nullam urna nisl, aliquet sit amet luctus sit amet, molestie id sem.	1
10	RF10	Skis alpin JO	10	HEAD	skis_alpin/jo	Pellentesque porta pretium nibh. Nulla sed metus purus. Fusce ullamcorper ex vitae nisl vehicula, sed convallis odio aliquet. Morbi varius quis metus ut vestibulum.	1
12	RF12	Skis alpin N6	12	HEAD	skis_alpin/n6	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam finibus hendrerit mattis. Pellentesque ut elit sollicitudin, iaculis quam vitae, maximus nulla.	1
9	RF9	Skis alpin Enfants	9	POW	skis_alpin/enfants	Pellentesque porta pretium nibh. Nulla sed metus purus. Fusce ullamcorper ex vitae nisl vehicula, sed convallis odio aliquet. Morbi varius quis metus ut vestibulum.	1
15	RF15	Skis alpin 360P	15	BLIZZARD	skis_alpin/360p	Curabitur venenatis ex vitae mollis cursus. Vivamus nisl tellus, euismod in leo vitae, rhoncus pellentesque nisl. 	1
1	RF1	Skis alpin PRO	1	NEFF	skis_alpin/pro	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam finibus hendrerit mattis. Pellentesque ut elit sollicitudin, iaculis quam vitae, maximus nulla.	1
2	RF2	Skis alpin XTrem	2	NEFF	skis_alpin/xtrem	Integer at lorem dolor. Praesent a facilisis lectus, in eleifend dui. Fusce gravida, velit ac tincidunt mollis, leo dui euismod leo, a consequat eros urna nec leo. Nullam urna nisl, aliquet sit amet luctus sit amet, molestie id sem.	1
4	RF4	Skis alpin Découverte	4	SCOTT	skis_alpin/découverte	Integer at lorem dolor. Praesent a facilisis lectus, in eleifend dui. Fusce gravida, velit ac tincidunt mollis, leo dui euismod leo, a consequat eros urna nec leo. Nullam urna nisl, aliquet sit amet luctus sit amet, molestie id sem.	1
6	RF6	Skis alpin Compét	6	POW	skis_alpin/compét	Curabitur venenatis ex vitae mollis cursus. Vivamus nisl tellus, euismod in leo vitae, rhoncus pellentesque nisl. 	1
3	RF3	Skis alpin Rossignol	3	Rossignol	skis_alpin/rossignol	Curabitur venenatis ex vitae mollis cursus. Vivamus nisl tellus, euismod in leo vitae, rhoncus pellentesque nisl. 	1
5	RF5	Skis alpin X12	5	SCOTT	skis_alpin/x12	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam finibus hendrerit mattis. Pellentesque ut elit sollicitudin, iaculis quam vitae, maximus nulla.	1
\.


--
-- Data for Name: price; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.price ("idPrice", price, date) FROM stdin;
1	19.9899999999999984	2019-02-16
2	39.990000000000002	2019-02-16
3	49.990000000000002	2019-02-16
4	99.9899999999999949	2019-02-16
5	149.990000000000009	2019-02-16
6	159.990000000000009	2019-02-16
7	199.990000000000009	2019-02-16
8	299.990000000000009	2019-02-16
9	399.990000000000009	2019-02-16
10	499.990000000000009	2019-02-16
\.


--
-- Data for Name: equipment; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.equipment ("idModel", "idEquipment", "idStore", "idPrice", "shoeSize", size) FROM stdin;
1	1	1	7	42	\N
1	2	1	7	42	\N
1	3	1	7	44	\N
1	4	1	7	44	\N
1	5	1	7	44	\N
1	6	1	7	44	\N
1	7	1	7	48	\N
1	8	1	7	48	\N
1	9	1	7	48	\N
1	10	1	7	48	\N
2	11	1	7	44	\N
2	12	1	7	44	\N
2	13	1	7	44	\N
2	14	1	7	44	\N
2	15	1	7	44	\N
2	16	1	7	46	\N
2	17	1	7	46	\N
2	18	1	7	46	\N
2	19	1	7	46	\N
2	20	1	7	46	\N
3	21	1	4	40	\N
3	22	1	4	40	\N
3	23	1	4	40	\N
3	24	1	4	40	\N
3	25	1	4	42	\N
3	26	1	4	44	\N
3	27	1	4	48	\N
3	28	1	4	48	\N
3	29	1	4	48	\N
3	30	1	4	48	\N
2	31	1	7	42	\N
2	32	1	7	42	\N
1	33	1	7	42	\N
\.


--
-- Data for Name: booking_equipment; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.booking_equipment ("idBooking", "idEquipment", "startDate", "endDate") FROM stdin;
\.


--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
1	malik	migrations.xml	2019-02-16 14:43:05.333149	1	EXECUTED	8:ffce1fecdb92234552770eeb1a8af8ef	createTable tableName=store; createTable tableName=price; createTable tableName=model; createTable tableName=equipment; createTable tableName=status_equipment; sql; createTable tableName=booking; createTable tableName=booking_equipment; createTabl...		\N	3.6.1	\N	\N	0328185177
2	malik	migrations.xml	2019-02-16 14:43:05.369747	2	EXECUTED	8:8d366a640bcc35eeddf11b6b5460bed5	createTable tableName=insurance; createTable tableName=subscribe_insurance; addPrimaryKey constraintName=pk_booking_insurance, tableName=subscribe_insurance		\N	3.6.1	\N	\N	0328185177
1	malik	migrations.xml	2019-02-16 14:43:05.333149	1	EXECUTED	8:ffce1fecdb92234552770eeb1a8af8ef	createTable tableName=store; createTable tableName=price; createTable tableName=model; createTable tableName=equipment; createTable tableName=status_equipment; sql; createTable tableName=booking; createTable tableName=booking_equipment; createTabl...		\N	3.6.1	\N	\N	0328185177
2	malik	migrations.xml	2019-02-16 14:43:05.369747	2	EXECUTED	8:8d366a640bcc35eeddf11b6b5460bed5	createTable tableName=insurance; createTable tableName=subscribe_insurance; addPrimaryKey constraintName=pk_booking_insurance, tableName=subscribe_insurance		\N	3.6.1	\N	\N	0328185177
1	malik	migrations.xml	2019-02-16 14:43:05.333149	1	EXECUTED	8:ffce1fecdb92234552770eeb1a8af8ef	createTable tableName=store; createTable tableName=price; createTable tableName=model; createTable tableName=equipment; createTable tableName=status_equipment; sql; createTable tableName=booking; createTable tableName=booking_equipment; createTabl...		\N	3.6.1	\N	\N	0328185177
2	malik	migrations.xml	2019-02-16 14:43:05.369747	2	EXECUTED	8:8d366a640bcc35eeddf11b6b5460bed5	createTable tableName=insurance; createTable tableName=subscribe_insurance; addPrimaryKey constraintName=pk_booking_insurance, tableName=subscribe_insurance		\N	3.6.1	\N	\N	0328185177
1	malik	migrations.xml	2019-02-16 14:43:05.333149	1	EXECUTED	8:ffce1fecdb92234552770eeb1a8af8ef	createTable tableName=store; createTable tableName=price; createTable tableName=model; createTable tableName=equipment; createTable tableName=status_equipment; sql; createTable tableName=booking; createTable tableName=booking_equipment; createTabl...		\N	3.6.1	\N	\N	0328185177
2	malik	migrations.xml	2019-02-16 14:43:05.369747	2	EXECUTED	8:8d366a640bcc35eeddf11b6b5460bed5	createTable tableName=insurance; createTable tableName=subscribe_insurance; addPrimaryKey constraintName=pk_booking_insurance, tableName=subscribe_insurance		\N	3.6.1	\N	\N	0328185177
1	malik	migrations.xml	2019-02-16 14:43:05.333149	1	EXECUTED	8:ffce1fecdb92234552770eeb1a8af8ef	createTable tableName=store; createTable tableName=price; createTable tableName=model; createTable tableName=equipment; createTable tableName=status_equipment; sql; createTable tableName=booking; createTable tableName=booking_equipment; createTabl...		\N	3.6.1	\N	\N	0328185177
2	malik	migrations.xml	2019-02-16 14:43:05.369747	2	EXECUTED	8:8d366a640bcc35eeddf11b6b5460bed5	createTable tableName=insurance; createTable tableName=subscribe_insurance; addPrimaryKey constraintName=pk_booking_insurance, tableName=subscribe_insurance		\N	3.6.1	\N	\N	0328185177
1	malik	migrations.xml	2019-02-16 14:43:05.333149	1	EXECUTED	8:ffce1fecdb92234552770eeb1a8af8ef	createTable tableName=store; createTable tableName=price; createTable tableName=model; createTable tableName=equipment; createTable tableName=status_equipment; sql; createTable tableName=booking; createTable tableName=booking_equipment; createTabl...		\N	3.6.1	\N	\N	0328185177
2	malik	migrations.xml	2019-02-16 14:43:05.369747	2	EXECUTED	8:8d366a640bcc35eeddf11b6b5460bed5	createTable tableName=insurance; createTable tableName=subscribe_insurance; addPrimaryKey constraintName=pk_booking_insurance, tableName=subscribe_insurance		\N	3.6.1	\N	\N	0328185177
1	malik	migrations.xml	2019-02-16 14:43:05.333149	1	EXECUTED	8:ffce1fecdb92234552770eeb1a8af8ef	createTable tableName=store; createTable tableName=price; createTable tableName=model; createTable tableName=equipment; createTable tableName=status_equipment; sql; createTable tableName=booking; createTable tableName=booking_equipment; createTabl...		\N	3.6.1	\N	\N	0328185177
2	malik	migrations.xml	2019-02-16 14:43:05.369747	2	EXECUTED	8:8d366a640bcc35eeddf11b6b5460bed5	createTable tableName=insurance; createTable tableName=subscribe_insurance; addPrimaryKey constraintName=pk_booking_insurance, tableName=subscribe_insurance		\N	3.6.1	\N	\N	0328185177
1	malik	migrations.xml	2019-02-16 14:43:05.333149	1	EXECUTED	8:ffce1fecdb92234552770eeb1a8af8ef	createTable tableName=store; createTable tableName=price; createTable tableName=model; createTable tableName=equipment; createTable tableName=status_equipment; sql; createTable tableName=booking; createTable tableName=booking_equipment; createTabl...		\N	3.6.1	\N	\N	0328185177
2	malik	migrations.xml	2019-02-16 14:43:05.369747	2	EXECUTED	8:8d366a640bcc35eeddf11b6b5460bed5	createTable tableName=insurance; createTable tableName=subscribe_insurance; addPrimaryKey constraintName=pk_booking_insurance, tableName=subscribe_insurance		\N	3.6.1	\N	\N	0328185177
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
\.


--
-- Data for Name: insurance; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.insurance ("idInsurance", name, "insuranceRate", price, "isActive", "idStore") FROM stdin;
\.


--
-- Data for Name: status_equipment; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.status_equipment ("idEquipment", status, date) FROM stdin;
\.


--
-- Data for Name: subscribe_insurance; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.subscribe_insurance ("idInsurance", "idEquipment", "idBooking") FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public."user" ("idUser", "lastName", email, "idStore", "firstName", "isAdmin", "arrivalDate", "departureDate") FROM stdin;
\.


--
-- Name: booking_idBooking_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public."booking_idBooking_seq"', 1, false);


--
-- Name: equipment_idEquipment_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public."equipment_idEquipment_seq"', 1, false);


--
-- Name: insurance_idInsurance_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public."insurance_idInsurance_seq"', 1, false);


--
-- Name: model_idModel_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public."model_idModel_seq"', 1, false);


--
-- Name: price_idPrice_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public."price_idPrice_seq"', 1, false);


--
-- Name: store_idStore_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public."store_idStore_seq"', 1, false);


--
-- Name: user_idUser_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public."user_idUser_seq"', 1, false);


--
-- PostgreSQL database dump complete
--

